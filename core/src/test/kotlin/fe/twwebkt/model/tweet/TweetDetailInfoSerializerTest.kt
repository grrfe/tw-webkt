package fe.twwebkt.model.tweet

import assertk.assertThat
import assertk.assertions.isEqualTo
import assertk.tableOf
import com.google.gson.JsonObject
import fe.gson.util.Json
import fe.twwebkt.model.user.TwitterUser
import fe.twwebkt.serializer.tweet.TweetDetailInfoSerializer
import fe.twwebkt.serializer.user.TwitterUserSerializer
import java.time.LocalDateTime
import kotlin.test.Test

internal class TweetDetailInfoSerializerTest {
    private val tweetDetailInfoSerializer = TweetDetailInfoSerializer(twitterUserSerializer = TwitterUserSerializer())
    private val tweetJson = """
    {
      "itemContent": {
        "itemType": "TimelineTweet",
        "__typename": "TimelineTweet",
        "tweet_results": {
          "result": {
            "__typename": "Tweet",
            "rest_id": "1869841169917923802",
            "core": {
              "user_results": {
                "result": {
                  "__typename": "User",
                  "id": "VXNlcjo3MDYwOTg3MTg3MjcxNTE2MTY=",
                  "rest_id": "706098718727151616",
                  "affiliates_highlighted_label": {},
                  "has_graduated_access": true,
                  "is_blue_verified": false,
                  "profile_image_shape": "Circle",
                  "legacy": {
                    "can_dm": true,
                    "can_media_tag": false,
                    "created_at": "Sat Mar 05 12:47:08 +0000 2016",
                    "default_profile": false,
                    "default_profile_image": false,
                    "description": "Saudi Military Opposition • Germany chases female Saudi asylum seekers, inside and outside Germany, to destroy their lives • Germany wants to islamize Europe 🚫",
                    "entities": {
                      "description": {
                        "urls": []
                      },
                      "url": {
                        "urls": [
                          {
                            "display_url": "wearesaudis.net",
                            "expanded_url": "https://wearesaudis.net",
                            "url": "https://t.co/pH3wrPS0Br",
                            "indices": [
                              0,
                              23
                            ]
                          }
                        ]
                      }
                    },
                    "fast_followers_count": 0,
                    "favourites_count": 730,
                    "followers_count": 48210,
                    "friends_count": 1081,
                    "has_custom_timelines": true,
                    "is_translator": false,
                    "listed_count": 174,
                    "location": "Germany",
                    "media_count": 5184,
                    "name": "Taleb Al Abdulmohsen",
                    "normal_followers_count": 48210,
                    "pinned_tweet_ids_str": [
                      "1823443639894466672"
                    ],
                    "possibly_sensitive": false,
                    "profile_banner_url": "https://pbs.twimg.com/profile_banners/706098718727151616/1704426751",
                    "profile_image_url_https": "https://pbs.twimg.com/profile_images/706100946925916160/hokGF9NQ_normal.jpg",
                    "profile_interstitial_type": "",
                    "screen_name": "DrTalebJawad",
                    "statuses_count": 121368,
                    "translator_type": "none",
                    "url": "https://t.co/pH3wrPS0Br",
                    "verified": false,
                    "want_retweets": false,
                    "withheld_in_countries": []
                  },
                  "professional": {
                    "rest_id": "1468089852650418178",
                    "professional_type": "Creator",
                    "category": []
                  }
                }
              }
            },
            "unmention_data": {},
            "edit_control": {
              "edit_tweet_ids": [
                "1869841169917923802"
              ],
              "editable_until_msecs": "1734643449534",
              "is_edit_eligible": false,
              "edits_remaining": "5"
            },
            "is_translatable": true,
            "views": {
              "count": "35",
              "state": "EnabledWithCount"
            },
            "source": "<a href=\"http://twitter.com/download/android\" rel=\"nofollow\">Twitter for Android</a>",
            "legacy": {
              "bookmark_count": 0,
              "bookmarked": false,
              "created_at": "Thu Dec 19 20:24:09 +0000 2024",
              "conversation_id_str": "1869841169917923802",
              "display_text_range": [
                0,
                140
              ],
              "entities": {
                "user_mentions": [
                  {
                    "id_str": "248148576",
                    "name": "طارق بن عزيز",
                    "screen_name": "t_alaziz",
                    "indices": [
                      3,
                      12
                    ]
                  }
                ],
                "urls": [],
                "hashtags": [],
                "symbols": []
              },
              "favorite_count": 0,
              "favorited": false,
              "full_text": "RT @t_alaziz: قضية شادن تجسد التناقض الصارخ في السعودية؛ بينما تُستخدم فتيات يعملن في مجال الترفيه الجنسي للبالغين ويرتدين فنايل أندية رياض…",
              "is_quote_status": true,
              "lang": "ar",
              "quote_count": 0,
              "quoted_status_id_str": "1869821841503588551",
              "quoted_status_permalink": {
                "url": "https://t.co/C7bXemtFBU",
                "expanded": "https://twitter.com/shvdinn/status/1869821841503588551",
                "display": "x.com/shvdinn/status…"
              },
              "reply_count": 0,
              "retweet_count": 21,
              "retweeted": false,
              "user_id_str": "706098718727151616",
              "id_str": "1869841169917923802",
              "retweeted_status_result": {
                "result": {
                  "__typename": "Tweet",
                  "rest_id": "1869840933514293265",
                  "core": {
                    "user_results": {
                      "result": {
                        "__typename": "User",
                        "id": "VXNlcjoyNDgxNDg1NzY=",
                        "rest_id": "248148576",
                        "affiliates_highlighted_label": {},
                        "has_graduated_access": true,
                        "is_blue_verified": true,
                        "profile_image_shape": "Circle",
                        "legacy": {
                          "can_dm": true,
                          "can_media_tag": false,
                          "created_at": "Sun Feb 06 10:48:11 +0000 2011",
                          "default_profile": true,
                          "default_profile_image": false,
                          "description": "سُجنت وحُكم على حسابي بالإغلاق لدفاعي عن حقوق الـ LGBTQ+ وحرية التعبير والمعتقد والحريات الفردية/🏳️‍⚧️🏳️‍🌈/أحلم بالعدالة/ماجستير قانون دولي في حقوق الإنسان",
                          "entities": {
                            "description": {
                              "urls": []
                            },
                            "url": {
                              "urls": [
                                {
                                  "display_url": "snapchat.com/t/T3e8P9YS",
                                  "expanded_url": "https://snapchat.com/t/T3e8P9YS",
                                  "url": "https://t.co/J38uQaFbJ8",
                                  "indices": [
                                    0,
                                    23
                                  ]
                                }
                              ]
                            }
                          },
                          "fast_followers_count": 0,
                          "favourites_count": 27933,
                          "followers_count": 47766,
                          "friends_count": 333,
                          "has_custom_timelines": false,
                          "is_translator": false,
                          "listed_count": 95,
                          "location": "السعودية/الولايات المتحدة",
                          "media_count": 2752,
                          "name": "طارق بن عزيز",
                          "normal_followers_count": 47766,
                          "pinned_tweet_ids_str": [
                            "674292128240025601"
                          ],
                          "possibly_sensitive": false,
                          "profile_banner_url": "https://pbs.twimg.com/profile_banners/248148576/1569684182",
                          "profile_image_url_https": "https://pbs.twimg.com/profile_images/1791632883922472960/QKMqgD5m_normal.jpg",
                          "profile_interstitial_type": "",
                          "screen_name": "t_alaziz",
                          "statuses_count": 41702,
                          "translator_type": "none",
                          "url": "https://t.co/J38uQaFbJ8",
                          "verified": false,
                          "want_retweets": false,
                          "withheld_in_countries": []
                        }
                      }
                    }
                  },
                  "unmention_data": {},
                  "edit_control": {
                    "edit_tweet_ids": [
                      "1869840933514293265"
                    ],
                    "editable_until_msecs": "1734643393000",
                    "is_edit_eligible": true,
                    "edits_remaining": "5"
                  },
                  "is_translatable": true,
                  "views": {
                    "count": "1607161",
                    "state": "EnabledWithCount"
                  },
                  "source": "<a href=\"http://twitter.com/download/iphone\" rel=\"nofollow\">Twitter for iPhone</a>",
                  "note_tweet": {
                    "is_expandable": true,
                    "note_tweet_results": {
                      "result": {
                        "id": "Tm90ZVR3ZWV0OjE4Njk4NDA5MzM0MzQ2MDE0NzQ=",
                        "text": "قضية شادن تجسد التناقض الصارخ في السعودية؛ بينما تُستخدم فتيات يعملن في مجال الترفيه الجنسي للبالغين ويرتدين فنايل أندية رياضية سعودية للترويج لصورة الدولة عالمياً، يتم سجن فتاة سعودية لمجرد الاشتباه بأنها مارست الترفيه الجنسي!     شادن ليست مجرد ضحية لهذا الظلم، بل هي صوت يفضح ازدواجية حكومة تسوّق الانفتاح والتغيير للعالم بينما تستمر في قمع الحريات داخلياً..     شادن ليست الحالة الوحيدة، لكن صوتها مهم لأنه يسلط الضوء على معاناة الكثيرين ممن يعيشون تحت هذه الانتهاكات، التي تُرتكب باسم القيم الاجتماعية والدين، بينما يمنعون من الدفاع عن أنفسهم أو الحديث عن الظلم الذي يواجهونه!",
                        "entity_set": {
                          "user_mentions": [],
                          "urls": [],
                          "hashtags": [],
                          "symbols": []
                        }
                      }
                    }
                  },
                  "quoted_status_result": {},
                  "legacy": {
                    "bookmark_count": 332,
                    "bookmarked": false,
                    "created_at": "Thu Dec 19 20:23:13 +0000 2024",
                    "conversation_id_str": "1869840933514293265",
                    "display_text_range": [
                      0,
                      275
                    ],
                    "entities": {
                      "user_mentions": [],
                      "urls": [],
                      "hashtags": [],
                      "symbols": []
                    },
                    "favorite_count": 135,
                    "favorited": false,
                    "full_text": "قضية شادن تجسد التناقض الصارخ في السعودية؛ بينما تُستخدم فتيات يعملن في مجال الترفيه الجنسي للبالغين ويرتدين فنايل أندية رياضية سعودية للترويج لصورة الدولة عالمياً، يتم سجن فتاة سعودية لمجرد الاشتباه بأنها مارست الترفيه الجنسي!     شادن ليست مجرد ضحية لهذا الظلم، بل هي صوت يفضح",
                    "is_quote_status": true,
                    "lang": "ar",
                    "quote_count": 10,
                    "quoted_status_id_str": "1869821841503588551",
                    "quoted_status_permalink": {
                      "url": "https://t.co/C7bXemtFBU",
                      "expanded": "https://twitter.com/shvdinn/status/1869821841503588551",
                      "display": "x.com/shvdinn/status…"
                    },
                    "reply_count": 92,
                    "retweet_count": 21,
                    "retweeted": false,
                    "user_id_str": "248148576",
                    "id_str": "1869840933514293265"
                  }
                }
              }
            }
          }
        },
        "tweetDisplayType": "Tweet"
      }
    }
    """.trimIndent()

    @Test
    fun `test unwrapContainer`() {
        tableOf("input", "expected")
            .row(
                """
                {
                  "legacy": {}
                }
                """.trimIndent(),
                """
                {
                  "legacy": {}
                }
                """.trimIndent()
            )
            .row(
                """
                {
                  "tweet": {}
                }
                """.trimIndent(),
                """{}"""
            )
            .forAll { input, expected ->
                val result = tweetDetailInfoSerializer.unwrapContainer(Json.parseJson(input))

                assertThat(result).isEqualTo(Json.parseJson<JsonObject>(expected))
            }
    }

    @Test
    fun `test tweet source parser`() {
        tableOf("source", "expected")
            .row(
                "<a href=\"http://twitter.com/download/iphone\" rel=\"nofollow\">Twitter for iPhone</a>",
                PostSource.iPhone
            )
            .row(
                "<a href=\"http://twitter.com/download/iphone\" rel=\"nofollow\">Twitter for iPad</a>",
                PostSource.iPad
            )
            .row(
                "<a href=\"http://twitter.com/download/android\" rel=\"nofollow\">Twitter for Android</a>",
                PostSource.Android
            )
            .row(
                "<a href=\"https://mobile.twitter.com\" rel=\"nofollow\">Twitter Web App</a>",
                PostSource.Web
            )
            .row(
                "<a href=\"http://twitter.com/download/iphone\" rel=\"nofollow\">Dummy</a>",
                PostSource.Unknown
            )
            .forAll { source, expected ->
                assertThat(tweetDetailInfoSerializer.parseSource(source)).isEqualTo(expected)
            }
    }

    @Test
    fun `handle timeline tweet`() {
        val result = tweetDetailInfoSerializer.handleTweet(Json.parseJson(tweetJson))
        val expected = TweetDetailInfo(
            tweetId = "1869841169917923802",
            createdAt = LocalDateTime.of(2024, 12, 19, 20, 24, 9),
            source = PostSource.Android,
            user = TwitterUser(
                userId = "706098718727151616",
                name = "Taleb Al Abdulmohsen",
                screenName = "DrTalebJawad",
                description = "Saudi Military Opposition • Germany chases female Saudi asylum seekers, inside and outside Germany, to destroy their lives • Germany wants to islamize Europe 🚫",
                createdAt = LocalDateTime.of(2016, 3, 5, 12, 47, 8),
                location = "Germany",
                mediaCount = 5184,
                statusCount = null,
                followers = 48210,
                following = 1081
            ),
            conversationId = "1869841169917923802",
            replyToId = null,
            favorites = 0,
            retweets = 21,
            views = 35,
            text = "RT @t_alaziz: قضية شادن تجسد التناقض الصارخ في السعودية؛ بينما تُستخدم فتيات يعملن في مجال الترفيه الجنسي للبالغين ويرتدين فنايل أندية رياض…",
            media = null,
            retweetedTweet = TweetDetailInfo(
                tweetId = "1869840933514293265",
                createdAt = LocalDateTime.of(2024, 12, 19, 20, 23, 13),
                user = TwitterUser(
                    userId = "248148576",
                    name = "طارق بن عزيز",
                    screenName = "t_alaziz",
                    description = "سُجنت وحُكم على حسابي بالإغلاق لدفاعي عن حقوق الـ LGBTQ+ وحرية التعبير والمعتقد والحريات الفردية/🏳️‍⚧️🏳️‍🌈/أحلم بالعدالة/ماجستير قانون دولي في حقوق الإنسان",
                    createdAt = LocalDateTime.of(2011, 2, 6, 10, 48, 11),
                    location = "السعودية/الولايات المتحدة",
                    mediaCount = 2752,
                    statusCount = null,
                    followers = 47766,
                    following = 333
                ),
                source = PostSource.iPhone,
                conversationId = "1869840933514293265",
                replyToId = null,
                favorites = 135,
                retweets = 21,
                views = 1607161,
                text = "قضية شادن تجسد التناقض الصارخ في السعودية؛ بينما تُستخدم فتيات يعملن في مجال الترفيه الجنسي للبالغين ويرتدين فنايل أندية رياضية سعودية للترويج لصورة الدولة عالمياً، يتم سجن فتاة سعودية لمجرد الاشتباه بأنها مارست الترفيه الجنسي!     شادن ليست مجرد ضحية لهذا الظلم، بل هي صوت يفضح",
                media = null,
                retweetedTweet = null,
                mediaOnlyTweet = false,
                tweetTextSingleUrl = false
            ),
            mediaOnlyTweet = false,
            tweetTextSingleUrl = false
        )

        assertThat(result).isEqualTo(expected)
    }

    private val tweetJson2 = """
    {
      "data": {
        "tweet_result": {
          "result": {
            "__typename": "Tweet",
            "rest_id": "1890186229842661414",
            "view_count_info": {
              "count": "386181",
              "state": "EnabledWithCount"
            },
            "legacy": {
              "conversation_id_str": "1890186229842661414",
              "created_at": "Thu Feb 13 23:48:09 +0000 2025",
              "display_text_range": [
                0,
                53
              ],
              "entities": {
                "hashtags": [],
                "symbols": [],
                "urls": [],
                "user_mentions": [],
                "timestamps": []
              },
              "extended_entities": {
                "media": [
                  {
                    "display_url": "pic.x.com/sDtWR8cCkq",
                    "expanded_url": "https://x.com/ericmigi/status/1890186229842661414/photo/1",
                    "ext_media_availability": {
                      "status": "Available"
                    },
                    "features": {
                      "large": {
                        "faces": []
                      }
                    },
                    "id_str": "1890186107553476611",
                    "indices": [
                      54,
                      77
                    ],
                    "media_key": "3_1890186107553476611",
                    "media_url_https": "https://pbs.twimg.com/media/GjtKQAwacAMjKVP.jpg",
                    "original_info": {
                      "width": 1166,
                      "height": 648,
                      "focus_rects": [
                        {
                          "x": 9,
                          "y": 0,
                          "w": 1157,
                          "h": 648
                        },
                        {
                          "x": 462,
                          "y": 0,
                          "w": 648,
                          "h": 648
                        },
                        {
                          "x": 502,
                          "y": 0,
                          "w": 568,
                          "h": 648
                        },
                        {
                          "x": 624,
                          "y": 0,
                          "w": 324,
                          "h": 648
                        },
                        {
                          "x": 0,
                          "y": 0,
                          "w": 1166,
                          "h": 648
                        }
                      ]
                    },
                    "sizes": {
                      "large": {
                        "w": 1166,
                        "h": 648
                      }
                    },
                    "type": "photo",
                    "url": "https://t.co/sDtWR8cCkq"
                  },
                  {
                    "display_url": "pic.x.com/sDtWR8cCkq",
                    "expanded_url": "https://x.com/ericmigi/status/1890186229842661414/photo/1",
                    "ext_media_availability": {
                      "status": "Available"
                    },
                    "features": {
                      "large": {
                        "faces": []
                      }
                    },
                    "id_str": "1890186115153641472",
                    "indices": [
                      54,
                      77
                    ],
                    "media_key": "3_1890186115153641472",
                    "media_url_https": "https://pbs.twimg.com/media/GjtKQdEbwAAbmcr.png",
                    "original_info": {
                      "width": 856,
                      "height": 618,
                      "focus_rects": [
                        {
                          "x": 0,
                          "y": 139,
                          "w": 856,
                          "h": 479
                        },
                        {
                          "x": 226,
                          "y": 0,
                          "w": 618,
                          "h": 618
                        },
                        {
                          "x": 264,
                          "y": 0,
                          "w": 542,
                          "h": 618
                        },
                        {
                          "x": 381,
                          "y": 0,
                          "w": 309,
                          "h": 618
                        },
                        {
                          "x": 0,
                          "y": 0,
                          "w": 856,
                          "h": 618
                        }
                      ]
                    },
                    "sizes": {
                      "large": {
                        "w": 856,
                        "h": 618
                      }
                    },
                    "type": "photo",
                    "url": "https://t.co/sDtWR8cCkq"
                  }
                ]
              },
              "favorite_count": 5359,
              "favorited": false,
              "full_text": "Guess which Blackberry patent expires on Saturday.... https://t.co/sDtWR8cCkq",
              "is_quote_status": false,
              "lang": "en",
              "possibly_sensitive": false,
              "possibly_sensitive_editable": true,
              "quote_count": 94,
              "reply_count": 123,
              "retweet_count": 442,
              "retweeted": false,
              "user_id_str": "12286362",
              "bookmarked": false,
              "bookmark_count": 779
            },
            "core": {
              "user_result": {
                "result": {
                  "__typename": "User",
                  "rest_id": "12286362",
                  "is_blue_verified": true,
                  "profile_image_shape": "Circle",
                  "affiliates_highlighted_label": {},
                  "legacy": {
                    "can_dm": true,
                    "can_media_tag": false,
                    "advertiser_account_service_levels": [
                      "analytics"
                    ],
                    "advertiser_account_type": "promotable_user",
                    "analytics_type": "enabled",
                    "created_at": "Tue Jan 15 16:55:56 +0000 2008",
                    "description": "Product of my errors.  \nPreviously: @beeper @YCombinator @Pebble",
                    "entities": {
                      "url": {
                        "urls": [
                          {
                            "display_url": "ericmigi.com",
                            "expanded_url": "http://ericmigi.com",
                            "indices": [
                              0,
                              23
                            ],
                            "url": "https://t.co/VReAZPP1yI"
                          }
                        ]
                      },
                      "description": {
                        "hashtags": [],
                        "symbols": [],
                        "urls": [],
                        "user_mentions": [
                          {
                            "id_str": "0",
                            "indices": [
                              36,
                              43
                            ],
                            "name": "",
                            "screen_name": "beeper"
                          },
                          {
                            "id_str": "0",
                            "indices": [
                              44,
                              56
                            ],
                            "name": "",
                            "screen_name": "YCombinator"
                          },
                          {
                            "id_str": "0",
                            "indices": [
                              57,
                              64
                            ],
                            "name": "",
                            "screen_name": "Pebble"
                          }
                        ]
                      }
                    },
                    "fast_followers_count": 0,
                    "favourites_count": 6094,
                    "followers_count": 34463,
                    "friends_count": 1515,
                    "geo_enabled": true,
                    "has_custom_timelines": true,
                    "has_extended_profile": false,
                    "id_str": "12286362",
                    "is_translator": false,
                    "location": "Palo Alto, CA",
                    "media_count": 711,
                    "name": "Eric Migicovsky",
                    "normal_followers_count": 34463,
                    "pinned_tweet_ids_str": [
                      "1883970228574642629"
                    ],
                    "profile_background_color": "131516",
                    "profile_banner_url": "https://pbs.twimg.com/profile_banners/12286362/1599186290",
                    "profile_image_url_https": "https://pbs.twimg.com/profile_images/1014695017460318208/AIQ3Oqtb_normal.jpg",
                    "profile_interstitial_type": "",
                    "profile_link_color": "009999",
                    "protected": false,
                    "screen_name": "ericmigi",
                    "statuses_count": 6486,
                    "translator_type_enum": "None",
                    "url": "https://t.co/VReAZPP1yI",
                    "verified": false,
                    "withheld_in_countries": []
                  },
                  "super_follow_eligible": false,
                  "super_followed_by": false,
                  "super_following": false,
                  "exclusive_tweet_following": false
                }
              }
            },
            "edit_control": {
              "__typename": "EditControlInitial",
              "edit_tweet_ids": [
                "1890186229842661414"
              ],
              "editable_until_msecs": "1739494089000",
              "edits_remaining": "5",
              "is_edit_eligible": false
            },
            "unmention_data": {},
            "quick_promote_eligibility": {
              "eligibility": "IneligibleNotProfessional"
            },
            "conversation_muted": false,
            "is_translatable": true
          }
        }
      }
    }
    """

    @Test
    fun `handle tweet query`() {
        val result = tweetDetailInfoSerializer.handleTweet(Json.parseJson(tweetJson2))
        val expected = TweetDetailInfo(
            tweetId = "1890186229842661414",
            createdAt = LocalDateTime.of(2025, 2, 13, 23, 48, 9),
            source = PostSource.Unknown,
            user = TwitterUser(
                userId = "12286362",
                name = "Eric Migicovsky",
                screenName = "ericmigi",
                description = "Product of my errors.  \nPreviously: @beeper @YCombinator @Pebble",
                createdAt = LocalDateTime.of(2008, 1, 15, 16, 55, 56),
                location = "Palo Alto, CA",
                mediaCount = 711,
                statusCount = null,
                followers = 34463,
                following = 1515
            ),
            conversationId = "1890186229842661414",
            replyToId = null,
            favorites = 5359,
            retweets = 442,
            views = null,
            text = "Guess which Blackberry patent expires on Saturday.... https://t.co/sDtWR8cCkq",
            media = listOf(
                "https://pbs.twimg.com/media/GjtKQAwacAMjKVP.jpg",
                "https://pbs.twimg.com/media/GjtKQdEbwAAbmcr.png"
            ),
            mediaOnlyTweet = false,
            tweetTextSingleUrl = false
        )

        assertThat(result).isEqualTo(expected)
    }
}
