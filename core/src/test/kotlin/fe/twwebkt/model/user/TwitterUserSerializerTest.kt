package fe.twwebkt.model.user

import assertk.assertThat
import assertk.assertions.isEqualTo
import assertk.tableOf
import com.google.gson.JsonObject
import fe.gson.util.Json
import fe.twwebkt.serializer.user.TwitterUserSerializer
import kotlin.test.Test

internal class TwitterUserSerializerTest {
    private val twitterUserSerializer = TwitterUserSerializer()

    @Test
    fun `unwrap user profile`() {
        tableOf("root", "expected")
            .row<JsonObject, JsonObject>(
                Json.parseJson("""
                {
                  "data": {
                    "user": {
                      "result": {
                        "test__context": "Single user"
                      }
                    }
                  }
                }
                """.trimIndent()),
                Json.parseJson("""
                {
                  "test__context": "Single user"
                }
                """.trimIndent())
            )
            .row(
                Json.parseJson("""
                {
                  "itemContent": {
                    "user_results": {
                      "result": {
                        "test__context": "User timeline"
                      }
                    }
                  }
                }
                """.trimIndent()),
                Json.parseJson("""
                {
                  "test__context": "User timeline"
                }
                """.trimIndent())
            )
            .row(
                Json.parseJson("""
                {
                  "core": {
                    "user_results": {
                      "result": {
                        "test__context": "Tweet detail posted by user"
                      }
                    }
                  }
                }
                """.trimIndent()),
                Json.parseJson("""
                {
                  "test__context": "Tweet detail posted by user"
                }
                """.trimIndent())
            )
            .forAll { root, expected ->
                assertThat(twitterUserSerializer.unwrapUserResult(root)).isEqualTo(expected)
            }

    }
}
