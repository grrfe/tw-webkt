package fe.twwebkt.model

import assertk.assertThat
import assertk.assertions.isEqualTo
import com.google.gson.JsonObject
import fe.gson.dsl.jsonArray
import fe.gson.dsl.jsonObject
import fe.gson.util.Json
import fe.twwebkt.task.InstructionType
import fe.twwebkt.task.unwrapInstructions
import kotlin.test.Test

internal class UserTimelineV2SerializerTest {
    @Test
    fun test() {
        val userTweets = Json.parseJson<JsonObject>("""
        {
          "data": {
            "user": {
              "result": {
                "__typename": "User",
                "timeline_v2": {
                  "timeline": {
                    "instructions": [
                      {
                        "type": "TimelineClearCache"
                      },
                      {
                        "type": "TimelinePinEntry"
                      }
                    ]
                  }
                }
              }
            }
          }
        }""")

        val instructions = InstructionType.UserTimelineV2.unwrapInstructions(userTweets)
        val expected = jsonArray(
            jsonObject { "type" += "TimelineClearCache" },
            jsonObject { "type" += "TimelinePinEntry" }
        )

        assertThat(instructions).isEqualTo(expected)
    }
}
