package fe.twwebkt.test

import assertk.assertThat
import assertk.assertions.isEqualTo
import fe.twwebkt.task.error.RequestTaskErrorHandler
import fe.twwebkt.task.error.RequestTaskError
import kotlin.test.Test

internal class RequestErrorHandlerTest {
    @Test
    fun test() {
        assertThat(RequestTaskErrorHandler.handleError(429, "", "")).isEqualTo(RequestTaskError.RateLimited)
    }
}
