package fe.twwebkt.test

import fe.twwebkt.serializer.TwitterDateParser
import kotlin.test.Test
import kotlin.test.assertNotNull

internal class TwitterDateParserTest {
    @Test
    fun test() {
        assertNotNull(TwitterDateParser.parseOrNull("Fri Jul 28 18:21:09 +0000 2023"))
        assertNotNull(TwitterDateParser.parseOrNull("Sat Dec 07 13:33:25 +0000 2024"))
    }
}
