package fe.twwebkt

import fe.twwebkt.android.TwitterSessionHandle
import fe.twwebkt.store.FileSessionContextStore
import fe.twwebkt.store.SessionContextStore
import java.io.File

class ConsoleEntryPoint(
    private val sessionDir: File = File(".ignore"),
    private val store: SessionContextStore<String> = FileSessionContextStore(sessionDir),
) {
    fun main(args: Array<String>): TwitterSessionHandle {
        val username = args.firstOrNull() ?: error("First argument must be username!")

        val twitterContext = store.getTwitterContext(username)
        val storedAccount = store.loadAccount(username)

        val handle = twitterContext.login(storedAccount.account, storedAccount.session)
        store.save(username, handle)

        return handle
    }
}
