package fe.twwebkt.client.feature

sealed class Feature(val name: String, val default: Boolean) {
    companion object {
        val allFeatures = lazy {
            arrayOf(
                General,
                CreatorSubscriptions,
                LongForm,
                ResponsiveWeb,
                RWeb,
                SuperFollow,
                HiddenProfile,
                SubscriptionsVerification
            )
        }
    }
}

interface FeatureNamespace<Namespace : Feature> {
    val features: Lazy<Array<Namespace>>

    fun toMap(): Map<String, Boolean> {
        return features.value.associate { it.name to it.default }
    }
}

sealed class General(name: String, default: Boolean = true) : Feature(name, default) {
    data object HighlightsTweetsTabUiEnabled : General("highlights_tweets_tab_ui_enabled")
    data object StandardizedNudgesMisInfo : General("standardized_nudges_misinfo")
    data object TweetAwardsWebTipping : General("tweet_awards_web_tipping_enabled", false)
    data object TweetWithVisibilityResultsPreferGqlLimitedActionsPolicyEnabled : General(
        "tweet_with_visibility_results_prefer_gql_limited_actions_policy_enabled"
    )

    data object VerifiedPhoneLabel : General("verified_phone_label_enabled", false)
    data object ViewCountsEverywhereApi : General("view_counts_everywhere_api_enabled")
    data object ArticlesPreviewEnabled : General("articles_preview_enabled")
    data object C9sTweetAnatomyModeratorBadgeEnabled : General("c9s_tweet_anatomy_moderator_badge_enabled")
    data object CommunitiesWebEnableTweetCommunityResultsFetch : General(
        "communities_web_enable_tweet_community_results_fetch"
    )

    data object FreedomOfSpeechNotReachFetchEnabled : General("freedom_of_speech_not_reach_fetch_enabled")
    data object GraphqlIsTranslatableRwebTweetIsTranslatableEnabled : General(
        "graphql_is_translatable_rweb_tweet_is_translatable_enabled"
    )

    data object GraphqlTimelineV2BookmarkTimeline : General("graphql_timeline_v2_bookmark_timeline")
    data object ProfileLabelImprovementsPcfLabelInPostEnabled : General(
        "profile_label_improvements_pcf_label_in_post_enabled", false
    )

    data object AndroidGraphqlSkipApiMediaColorPalette : General("android_graphql_skip_api_media_color_palette")
    data object ArticlesApiEnabled : General("articles_api_enabled")
    data object BlueBusinessProfileImageShapeEnabled : General("blue_business_profile_image_shape_enabled")
    data object ImmersiveVideoStatusLinkableTimestamps : General("immersive_video_status_linkable_timestamps")

    data object TweetypieUnmentionOptimizationEnabled : General("tweetypie_unmention_optimization_enabled")
    data object PremiumContentApiReadEnabled : General("premium_content_api_read_enabled")
    data object ResponsiveWebGrokAnalyzeButtonFetchTrendsEnabled : General(
        "responsive_web_grok_analyze_button_fetch_trends_enabled"
    )

    companion object : FeatureNamespace<General> {
        override val features = lazy {
            arrayOf(
                HighlightsTweetsTabUiEnabled, StandardizedNudgesMisInfo, TweetAwardsWebTipping,
                TweetWithVisibilityResultsPreferGqlLimitedActionsPolicyEnabled, VerifiedPhoneLabel,
                ViewCountsEverywhereApi, ArticlesPreviewEnabled, C9sTweetAnatomyModeratorBadgeEnabled,
                CommunitiesWebEnableTweetCommunityResultsFetch, FreedomOfSpeechNotReachFetchEnabled,
                GraphqlIsTranslatableRwebTweetIsTranslatableEnabled, GraphqlTimelineV2BookmarkTimeline,
                ProfileLabelImprovementsPcfLabelInPostEnabled, AndroidGraphqlSkipApiMediaColorPalette,
                ArticlesApiEnabled, BlueBusinessProfileImageShapeEnabled, ImmersiveVideoStatusLinkableTimestamps,
                TweetypieUnmentionOptimizationEnabled,
                PremiumContentApiReadEnabled, ResponsiveWebGrokAnalyzeButtonFetchTrendsEnabled
            )
        }
    }
}

sealed class HiddenProfile(name: String, default: Boolean = true) : Feature("hidden_profile_$name", default) {
    data object SubscriptionsEnabled : HiddenProfile("subscriptions_enabled")
    data object LikesEnabled : HiddenProfile("likes_enabled")

    companion object : FeatureNamespace<HiddenProfile> {
        override val features = lazy {
            arrayOf(SubscriptionsEnabled, LikesEnabled)
        }
    }
}

sealed class SubscriptionsVerification(
    name: String, default: Boolean = true,
) : Feature("subscriptions_verification_info_$name", default) {

    data object Enabled : SubscriptionsVerification("enabled")
    data object VerifiedSinceEnabled : SubscriptionsVerification("verified_since_enabled")

    companion object : FeatureNamespace<SubscriptionsVerification> {
        override val features = lazy {
            arrayOf(Enabled, VerifiedSinceEnabled)
        }
    }
}


sealed class LongForm(name: String, default: Boolean = true) : Feature("longform_notetweets_$name", default) {
    data object Consumption : LongForm("consumption_enabled")
    data object InlineMedia : LongForm("inline_media_enabled")
    data object RichTextRead : LongForm("rich_text_read_enabled")

    companion object : FeatureNamespace<LongForm> {
        override val features = lazy {
            arrayOf(Consumption, InlineMedia, RichTextRead)
        }
    }
}


sealed class ResponsiveWeb(name: String, default: Boolean = true) : Feature("responsive_web_$name", default) {
    data object EditTweetApi : ResponsiveWeb("edit_tweet_api_enabled")
    data object EnhanceCards : ResponsiveWeb("enhance_cards_enabled")
    data object GraphqlExcludeDirective : ResponsiveWeb("graphql_exclude_directive_enabled")
    data object GraphqlSkipUserProfileImageExtensions : ResponsiveWeb(
        "graphql_skip_user_profile_image_extensions_enabled", false
    )

    data object GraphqlTimelineNavigation : ResponsiveWeb("graphql_timeline_navigation_enabled")
    data object TwitterArticleTweetConsumption : ResponsiveWeb("twitter_article_tweet_consumption_enabled")
    data object MediaDownloadVideoEnabled : ResponsiveWeb("media_download_video_enabled")

    companion object : FeatureNamespace<ResponsiveWeb> {
        override val features = lazy {
            arrayOf(
                EditTweetApi, EnhanceCards, GraphqlExcludeDirective, GraphqlSkipUserProfileImageExtensions,
                GraphqlTimelineNavigation, TwitterArticleTweetConsumption, MediaDownloadVideoEnabled
            )
        }
    }
}

sealed class CreatorSubscriptions(name: String, default: Boolean = true) : Feature(
    "creator_subscriptions_$name", default
) {
    data object QuoteTweetPreview : CreatorSubscriptions("quote_tweet_preview_enabled", false)
    data object TweetPreviewApi : CreatorSubscriptions("tweet_preview_api_enabled")

    companion object : FeatureNamespace<CreatorSubscriptions> {
        override val features = lazy {
            arrayOf(QuoteTweetPreview, TweetPreviewApi)
        }
    }
}

sealed class RWeb(name: String, default: Boolean = true) : Feature("rweb_$name", default) {
    data object TipJarConsumption : RWeb("tipjar_consumption_enabled")
    data object VideoTimestamps : RWeb("video_timestamps_enabled")
    data object ListsTimelineRedesignEnabled : RWeb("lists_timeline_redesign_enabled")

    companion object : FeatureNamespace<RWeb> {
        override val features = lazy {
            arrayOf(TipJarConsumption, VideoTimestamps, ListsTimelineRedesignEnabled)
        }
    }
}

sealed class SuperFollow(name: String, default: Boolean = true) : Feature("super_follow_$name", default) {
    data object BadgePrivacy : SuperFollow("badge_privacy_enabled", false)
    data object ExclusiveTweetNotifications : SuperFollow("exclusive_tweet_notifications_enabled")
    data object TweetApi : SuperFollow("tweet_api_enabled")
    data object UserApi : SuperFollow("user_api_enabled")

    companion object : FeatureNamespace<SuperFollow> {
        override val features = lazy {
            arrayOf(BadgePrivacy, ExclusiveTweetNotifications, TweetApi, UserApi)
        }
    }
}
