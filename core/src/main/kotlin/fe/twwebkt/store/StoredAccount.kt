package fe.twwebkt.store

import fe.twwebkt.android.device.KnownDeviceSession
import fe.twwebkt.task.request.login.TwitterAccount

data class StoredAccount(
    val account: TwitterAccount,
    val session: KnownDeviceSession?,
)
