package fe.twwebkt.store

import fe.twwebkt.android.TwitterContext
import fe.twwebkt.android.TwitterSessionHandle
import fe.twwebkt.android.device.KnownDeviceSession
import fe.twwebkt.task.request.login.TwitterAccount
import fe.twwebkt.util.decodeYaml
import fe.twwebkt.util.encodeYaml
import java.io.File
import kotlin.io.path.exists

interface SessionContextStore<Id> {
    fun getTwitterContext(identifier: Id): TwitterContext

    fun loadAccount(identifier: Id): StoredAccount

    fun save(identifier: Id, handle: TwitterSessionHandle)
}

class FileSessionContextStore(dir: File) : SessionContextStore<String> {
    // TODO: Decide on either Path or File
    private val dir = dir.toPath()

    companion object {
        private const val ACCOUNT = "account.yml"
        private const val INSTALLATION = "installation.yml"
        private const val SESSION = "session.yml"
    }

    override fun getTwitterContext(identifier: String): TwitterContext {
        val accountDir = dir.resolve(identifier)
        val installationConfig = accountDir.resolve(INSTALLATION)

        val context = installationConfig.decodeYaml<TwitterContext>()
        return context
    }

    override fun loadAccount(identifier: String): StoredAccount {
        val accountDir = dir.resolve(identifier)
        val accountConfig = accountDir.resolve(ACCOUNT)
        val sessionConfig = accountDir.resolve(SESSION)

        val account = accountConfig.decodeYaml<TwitterAccount>()

        val session = if (sessionConfig.exists()) {
            sessionConfig.decodeYaml<KnownDeviceSession>()
        } else {
            null
        }

        return StoredAccount(account, session)
    }

    override fun save(identifier: String, handle: TwitterSessionHandle) {
        val accountDir = dir.resolve(identifier)
        val sessionConfig = accountDir.resolve(SESSION)

        sessionConfig.encodeYaml(handle.request.session)
    }
}
