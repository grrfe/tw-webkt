package fe.twwebkt

import fe.gson.dsl.JsonObjectDslInit
import fe.gson.dsl.jsonObject
import fe.httpkt.HttpData
import fe.httpkt.json.JsonBody
import fe.httpkt.util.headersOf

sealed interface TwitterEndpoint {
    companion object {
        val BaseApiUrl = BaseUrl("https://api.twitter.com")
        val TwitterOAuth2SessionType: (HttpData.Builder.() -> Unit) = { headersOf("x-twitter-auth-type" to "OAuth2Session") }
    }

    val pathName: String
    val path: String

    fun build(url: String): String {
        return "$url/$pathName/$path"
    }

    sealed class TwitterV1Endpoint(override val path: String) : TwitterEndpoint {
        override val pathName = "1.1"
    }

    sealed class TwitterOauth2Endpoint(override val path: String) : TwitterEndpoint {
        override val pathName = "oauth2"
    }

    sealed class TwitterGraphqlEndpoint(private val queryId: String, name: String) : TwitterEndpoint {
        override val pathName = "graphql"
        override val path = "$queryId/$name"

        private val queryIdObj = jsonObject { "queryId" += queryId }

        fun jsonBody(init: JsonObjectDslInit): JsonBody {
            return JsonBody(jsonObject(queryIdObj, init))
        }
    }

    data object Activate : TwitterV1Endpoint("guest/activate.json")
    data object Onboarding : TwitterV1Endpoint("onboarding/task.json")
    data object CreateFriendships : TwitterV1Endpoint("friendships/create.json")
    data object VerifyCredentials : TwitterV1Endpoint("account/verify_credentials.json")
    data object ShowUser : TwitterV1Endpoint("users/show.json")

    data object Oauth2Token : TwitterOauth2Endpoint("token")

    data object Viewer : TwitterGraphqlEndpoint("k3027HdkVqbuDPpdoniLKA", "Viewer")
    data object UserByScreenName : TwitterGraphqlEndpoint("xc8f1g7BYqr6VTzTbvNlGw", "UserByScreenName")
    data object UserTweets : TwitterGraphqlEndpoint("TK4W-Bktk8AJk0L1QZnkrg", "UserTweets")
    data object UserMedia : TwitterGraphqlEndpoint("2EDA1hY0Ma1VYhfISprU3w", "UserMedia")
    data object GetUserClaims : TwitterGraphqlEndpoint("lFi3xnx0auUUnyG4YwpCNw", "GetUserClaims")
    data object Following : TwitterGraphqlEndpoint("t-BPOrMIduGUJWO_LxcvNQ", "Following")
    data object TweetDetail : TwitterGraphqlEndpoint("q94uRCEn65LZThakYcPT6g", "TweetDetail")
    data object TweetConversation : TwitterGraphqlEndpoint("GBSfDNBdZPRdJCfYd3mR7Q", "TweetDetail")
    data object TweetDelete : TwitterGraphqlEndpoint("VaenaVgh5q5ih7kvyVjgtg", "DeleteTweet")
    data object TweetResultByIdQuery : TwitterGraphqlEndpoint("3FWdAqNvUyx-Pt-O8JmNBw", "TweetResultByIdQuery")
    data object Bookmarks : TwitterGraphqlEndpoint("QUjXply7fA7fk05FRyajEg", "Bookmarks")
    data object BookmarkDelete : TwitterGraphqlEndpoint("Wlmlj2-xzyS1GN3a6cj-mQ", "DeleteBookmark")
}

@JvmInline
value class BaseUrl(val url: String) {
    fun atEndpoint(endpoint: TwitterEndpoint): String = endpoint.build(url)
}
