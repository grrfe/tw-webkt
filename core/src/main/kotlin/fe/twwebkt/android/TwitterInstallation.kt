package fe.twwebkt.android

import fe.kotlin.extension.string.encodeBase64Throw
import fe.twwebkt.util.randomHexString
import kotlinx.serialization.Serializable
import java.security.SecureRandom
import java.util.UUID
import kotlin.io.encoding.ExperimentalEncodingApi

@Serializable
data class TwitterInstallation(
    val clientUuid: String,
    val adId: String,
    val appSetId: String,
    val deviceId: String
) {
    companion object {
        private val random = SecureRandom()

        fun createRandom(): TwitterInstallation {
            val appId = randomHexString(16, random)

            return TwitterInstallation(
                UUID.randomUUID().toString(),
                UUID.randomUUID().toString(),
                UUID.randomUUID().toString(),
                appId
            )
        }
    }
}

@Serializable
data class TwitterVersion(
    val client: String = "TwitterAndroid",
    val version: String,
    val versionId: String,
    val apiKey: String,
    val apiSecret: String,
) {
    @OptIn(ExperimentalEncodingApi::class)
    val basicAuth by lazy { "$apiKey:$apiSecret".encodeBase64Throw() }
}
