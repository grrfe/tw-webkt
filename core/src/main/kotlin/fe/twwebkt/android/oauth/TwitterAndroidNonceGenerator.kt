package fe.twwebkt.android.oauth

import fe.httpkt.oauth.OAuthNonceGenerator
import java.security.SecureRandom
import kotlin.math.abs

data object TwitterAndroidNonceGenerator : OAuthNonceGenerator {
    private val random = SecureRandom()

    override fun generate(): String {
        return System.nanoTime().toString() + abs(random.nextLong()).toString()
    }
}
