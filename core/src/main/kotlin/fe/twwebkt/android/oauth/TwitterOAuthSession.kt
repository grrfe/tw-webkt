package fe.twwebkt.android.oauth

import fe.httpkt.oauth.OAuthContext
import fe.httpkt.oauth.OAuthNonceGenerator
import fe.httpkt.oauth.OAuthSession
import fe.httpkt.oauth.signing.HmacSHA1OAuthSigner
import fe.httpkt.oauth.signing.OAuthSigner
import fe.twwebkt.android.TwitterContext
import fe.twwebkt.android.util.TwitterHeaders
import fe.twwebkt.android.device.KnownDeviceSession

class TwitterOAuthSession(
    private val context: TwitterContext,
    val session: KnownDeviceSession,
    signer: OAuthSigner = HmacSHA1OAuthSigner,
    nonceGenerator: OAuthNonceGenerator = TwitterAndroidNonceGenerator,
    apiSecret: String = context.twitterVersion.apiSecret,
    oauthSecret: String = session.oauthSecret,
) : OAuthSession(
    oauthContext = OAuthContext(
        realm = "http://api.twitter.com/",
        token = session.oauthToken,
        consumerKey = context.twitterVersion.apiKey,
        signatureMethod = signer.method
    ),
    signer = signer,
    nonceGenerator = nonceGenerator,
    apiSecret = apiSecret,
    oauthSecret = oauthSecret,
    data = {
        addHeaders(context.headers)
        headers {
            TwitterHeaders.KNOWN_DEVICE_TOKEN += session.knownDeviceToken
        }
    }
)
