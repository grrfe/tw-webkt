package fe.twwebkt.android.device

import fe.std.string.joinToString
import kotlinx.serialization.Serializable
import java.net.URLEncoder
import java.nio.charset.StandardCharsets

@Serializable
data class DeviceModel(
    val manufacturer: String,
    val deviceName: String,
    val brand: String,
    val model: String,
    val displaySize: String,
    val memory: Long,
    val cpuCores: Int,
    val cpuClockSpeed: Long,
) {
    val deviceClass by lazy { DeviceClassEvaluator.calculate(memory, cpuCores, cpuClockSpeed) }
    val encodedDeviceName: String? by lazy { URLEncoder.encode(deviceName, StandardCharsets.UTF_8) }
    val modelStr by lazy {
        joinToString(manufacturer, encodedDeviceName, brand, model, "0", "", "1", "$deviceClass", separator = ";")
    }
}

@ConsistentCopyVisibility
@Serializable
data class Device internal constructor(val deviceModel: DeviceModel, val build: AndroidBuild) {
    val deviceString by lazy {
        "${deviceModel.encodedDeviceName}/${build.version.version} (${deviceModel.modelStr})"
    }
}

infix fun DeviceModel.withBuild(build: AndroidBuild): Device {
    return Device(this, build)
}
