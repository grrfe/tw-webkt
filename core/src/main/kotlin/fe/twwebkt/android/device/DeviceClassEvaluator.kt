package fe.twwebkt.android.device


object DeviceClassEvaluator {
    fun calculate(memory: Long, cpuCores: Int, cpuClockSpeed: Long): Int {
        if (memory != -1L) return fallback(memory, cpuCores, cpuClockSpeed)

        val list = listOfNotNull(
            checkCpuCores(cpuCores),
            checkCpuClockSpeed(cpuClockSpeed),
            checkMemory(memory)
        ).sorted()

        if (list.isEmpty()) {
            return -1
        }

        if (list.size % 2 != 0) {
            return list[list.size / 2]
        }

        val sizeHalf = (list.size / 2) - 1
        return ((list[sizeHalf + 1] - list[sizeHalf]) / 2) + list[sizeHalf]
    }

    private fun checkCpuCores(cpuCores: Int): Int? = when {
        cpuCores < 1 -> null
        cpuCores == 1 -> 2008
        cpuCores <= 3 -> 2011
        else -> 2012
    }

    private fun checkCpuClockSpeed(cpuClockSpeed: Long): Int? = when {
        cpuClockSpeed == -1L -> null
        cpuClockSpeed <= 528000 -> 2008
        cpuClockSpeed <= 620000 -> 2009
        cpuClockSpeed <= 1020000 -> 2010
        cpuClockSpeed <= 1220000 -> 2011
        cpuClockSpeed <= 1520000 -> 2012
        cpuClockSpeed <= 2020000 -> 2013
        else -> 2014
    }

    private fun checkMemory(memory: Long): Int? = when {
        memory <= 0 -> null
        memory <= 201326592 -> 2008
        memory <= 304087040 -> 2009
        memory <= 536870912 -> 2010
        memory <= 1073741824 -> 2011
        memory <= 1610612736 -> 2012
        memory <= 2147483648 -> 2013
        else -> 2014
    }

    private fun fallback(memory: Long, cpuCores: Int, cpuClockSpeed: Long): Int = when {
        memory <= 805306368 -> when (cpuCores) {
            in 0..1 -> 2009
            else -> 2010
        }

        memory <= 1073741824 -> when (cpuClockSpeed) {
            in 0..<1300000 -> 2011
            else -> 2012
        }

        memory <= 1610612736 -> when (cpuClockSpeed) {
            in 0..<1800000 -> 2012
            else -> 2013
        }

        memory <= 2147483648 -> 2013
        memory <= 3221225472 -> 2014
        memory <= 5368709120 -> 2015
        else -> 2016
    }
}
