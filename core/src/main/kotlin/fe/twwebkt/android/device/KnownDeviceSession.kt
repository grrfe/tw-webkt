package fe.twwebkt.android.device

import kotlinx.serialization.Serializable

@Serializable
data class KnownDeviceSession(
    val oauthToken: String,
    val oauthSecret: String,
    val knownDeviceToken: String,
)
