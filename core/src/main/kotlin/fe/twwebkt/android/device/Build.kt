package fe.twwebkt.android.device

import kotlinx.serialization.Serializable

@Serializable
data class AppLocale(
    val timezone: String,
    val simCountry: String,
    val language: String
)

@Serializable
data class AndroidBuild(
    val version: AndroidVersion,
    val id: String,
    val patchLevel: String
)

// https://apilevels.com/
enum class AndroidVersion(val version: String, val shortCode: String, val apiLevel: Int) {
    VanillaIceCream("15", "V", 35),
    UpsideDownCake("14", "U", 34),
    Tiramisu("13", "T", 33),
    SnowCone("12", "S_V2", 32),
    SnowConeV2("12", "S", 31),
    RedVelvetCake("11", "R", 30),
    QuinceTart("10", "Q", 29),
}
