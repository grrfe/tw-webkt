package fe.twwebkt.android

import fe.twwebkt.android.oauth.TwitterOAuthSession
import fe.twwebkt.task.request.login.TwitterAccount

data class TwitterSessionHandle(
    val request: TwitterOAuthSession,
    val account: TwitterAccount
)
