package fe.twwebkt.android.util

object TwitterHeaders {
    const val TIMEZONE = "Timezone"
    const val PATCH_LEVEL = "OS-Security-Patch-Level"
    const val ATTEST_TOKEN = "X-Attest-Token"
    const val CLIENT_UUID = "X-Client-UUID"
    const val API_VERSION = "X-Twitter-API-Version"
    const val CLIENT = "X-Twitter-Client"
    const val AD_ID = "X-Twitter-Client-AdID"
    const val ACTIVE_USER = "X-Twitter-Active-User"
    const val DEVICE_ID = "X-Twitter-Client-DeviceID"
    const val FLAVOR = "X-Twitter-Client-Flavor"
    const val LANGUAGE = "X-Twitter-Client-Language"
    const val LIMIT_TRACKING = "X-Twitter-Client-Limit-Ad-Tracking"
    const val CLIENT_VERSION = "X-Twitter-Client-Version"
    const val GUEST_TOKEN = "X-Guest-Token"
    const val OS_VERSION = "OS-Version"
    const val DISPLAY_SIZE = "Twitter-Display-Size"
    const val SYSTEM_UA = "System-User-Agent"
    const val APP_SET_ID = "X-Twitter-Client-AppSetID"
    const val KNOWN_DEVICE_TOKEN = "kdt"
    const val OPTIMIZE_BODY = "Optimize-Body"
    const val APP_TRACKING_TRANSPARENCY = "att"
}
