package fe.twwebkt.android

import fe.httpkt.Session
import fe.httpkt.util.headersOf
import fe.httpkt.data.Header
import fe.std.string.joinToString
import fe.twwebkt.android.device.AndroidBuild
import fe.twwebkt.android.device.AppLocale
import fe.twwebkt.android.device.Device
import fe.twwebkt.android.device.DeviceModel
import fe.twwebkt.android.device.withBuild
import fe.twwebkt.android.oauth.TwitterOAuthSession
import fe.twwebkt.android.util.TwitterHeaders
import fe.twwebkt.android.device.KnownDeviceSession
import fe.twwebkt.task.request.account.TwitterAccountVerifyCredentials
import fe.twwebkt.task.request.login.TwitterAccount
import fe.twwebkt.task.request.login.TwitterAndroidLoginFlow
import kotlinx.serialization.Serializable

@Serializable
class TwitterContext private constructor(
    private val device: Device,
    val twitterVersion: TwitterVersion,
    private val installation: TwitterInstallation,
    val locale: AppLocale,
) {
    val userAgent by lazy {
        joinToString(
            "${twitterVersion.client}/${twitterVersion.version}",
            "(${twitterVersion.versionId})",
            device.deviceString,
            separator = " "
        )
    }

    private val baseSystemUserAgent by lazy {
        joinToString(
            "Linux",
            device.build.version.shortCode,
            "Android ${device.build.version}",
            "${device.deviceModel.deviceName} Build/${device.build.id}",
            separator = "; "
        )
    }

    val headers by lazy {
        headersOf(
            "User-Agent" to userAgent,
            TwitterHeaders.CLIENT_VERSION to twitterVersion.version,
            TwitterHeaders.TIMEZONE to locale.timezone,
            TwitterHeaders.PATCH_LEVEL to device.build.patchLevel,
            TwitterHeaders.CLIENT_UUID to installation.clientUuid,
            TwitterHeaders.CLIENT to twitterVersion.client,
            TwitterHeaders.AD_ID to installation.adId,
            TwitterHeaders.DEVICE_ID to installation.deviceId,
            TwitterHeaders.LANGUAGE to locale.language,
            TwitterHeaders.LIMIT_TRACKING to "0",
            TwitterHeaders.OPTIMIZE_BODY to "true",
            TwitterHeaders.FLAVOR to "",
            TwitterHeaders.ACTIVE_USER to "yes",
            TwitterHeaders.API_VERSION to "5",
            TwitterHeaders.ATTEST_TOKEN to "no_token",
        )
    }

    fun createGuestLoginHeaders(guestToken: String): List<Header> {
        return headersOf(
            TwitterHeaders.GUEST_TOKEN to guestToken,
            TwitterHeaders.OS_VERSION to "${device.build.version.apiLevel}",
            TwitterHeaders.DISPLAY_SIZE to device.deviceModel.displaySize,
            TwitterHeaders.SYSTEM_UA to "Dalvik/2.1.0 ($baseSystemUserAgent)",
            TwitterHeaders.APP_SET_ID to installation.appSetId
        )
    }

    companion object {
        fun createDeviceContext(
            model: DeviceModel,
            build: AndroidBuild,
            twitter: TwitterVersion,
            installation: TwitterInstallation,
            locale: AppLocale,
        ): TwitterContext {
            return TwitterContext(model.withBuild(build), twitter, installation, locale)
        }
    }

    fun login(account: TwitterAccount, session: KnownDeviceSession? = null): TwitterSessionHandle {
        if (session != null) {
            val request = TwitterOAuthSession(this, session)
            val valid = runCatching { request.send(TwitterAccountVerifyCredentials) }
                .map { true }
                .getOrDefault(false)

            if (valid) {
                return TwitterSessionHandle(request, account)
            }
        }

        val flow = TwitterAndroidLoginFlow(this)
        val newSession = flow.execute(account, Session())

        return TwitterSessionHandle(TwitterOAuthSession(this, newSession), account)
    }
}
