package fe.twwebkt.api

import fe.httpkt.HttpData
import fe.httpkt.Request
import fe.httpkt.body.BodyData
import fe.httpkt.clazz.BodyRequest
import fe.httpkt.clazz.GetRequest
import fe.httpkt.ext.readToString
import fe.twwebkt.BaseUrl
import fe.twwebkt.TwitterEndpoint
import fe.twwebkt.task.error.RequestTaskError
import fe.twwebkt.task.error.RequestTaskErrorHandler
import java.net.HttpURLConnection

abstract class TwitterGetEndpoint<S>(
    baseUrl: BaseUrl = TwitterEndpoint.BaseApiUrl,
    endpoint: TwitterEndpoint,
    val endpointUrl: String = baseUrl.atEndpoint(endpoint),
    val parameters: Set<Parameter> = emptySet(),
    data: (HttpData.Builder.() -> Unit)? = null,
) : GetRequest<S, RequestTaskError>(endpointUrl, getParams = createGetParameters(parameters), data = data) {

    constructor(
        baseUrl: BaseUrl = TwitterEndpoint.BaseApiUrl,
        endpoint: TwitterEndpoint,
        variables: VariablesParameter = VariablesParameter.Empty,
        features: FeaturesParameter = FeaturesParameter.Empty,
        fieldToggles: FieldTogglesParameter = FieldTogglesParameter.Empty,
        data: (HttpData.Builder.() -> Unit)? = null,
    ) : this(
        baseUrl, endpoint,
        parameters = setOf(variables, features, fieldToggles),
        data = data
    )

    override fun handleError(con: HttpURLConnection): RequestTaskError {
        return RequestTaskErrorHandler.handleError(con.responseCode, con.contentType, con.readToString())
    }
}

abstract class TwitterBodyEndpoint<Endpoint : TwitterEndpoint, S>(
    method: Request.HttpMethod,
    baseUrl: BaseUrl = TwitterEndpoint.BaseApiUrl,
    endpoint: Endpoint,
    variables: VariablesParameter = VariablesParameter.Empty,
    features: FeaturesParameter = FeaturesParameter.Empty,
    fieldToggles: FieldTogglesParameter = FieldTogglesParameter.Empty,
    body: Endpoint.() -> BodyData,
    data: (HttpData.Builder.() -> Unit)? = null,
) : BodyRequest<S, RequestTaskError>(method = method, baseUrl.atEndpoint(endpoint), body = body(endpoint), data = data) {

    override fun handleError(con: HttpURLConnection): RequestTaskError {
        return RequestTaskErrorHandler.handleError(con.responseCode, con.contentType, con.readToString())
    }
}
