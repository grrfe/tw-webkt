package fe.twwebkt.api

import com.google.gson.Gson


interface Parameter {
    val name: String
    val map: Map<String, Any?>

    fun toGetParamString(gson: Gson): String {
        return gson.toJson(map.filterValues { it != null })
    }
}

fun createGetParameters(parameters: Set<Parameter>, gson: Gson = Gson()): Map<Any, Any> {
    return parameters.associate { it.name to it.toGetParamString(gson) }
}
