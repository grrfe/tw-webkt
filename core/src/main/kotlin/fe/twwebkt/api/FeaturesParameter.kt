package fe.twwebkt.api

import fe.twwebkt.client.feature.Feature

@JvmInline
value class FeaturesParameter(override val map: Map<String, Any?>) : Parameter {
    constructor(vararg values: Pair<String, Any?>) : this(values.toMap())

    override val name: String
        get() = "features"

    companion object {
        val Empty = FeaturesParameter()
        val Default by lazy {
            FeaturesParameter(map = Feature.allFeatures.value.map { it.toMap() }.merge())
        }
    }
}

@Deprecated(
    message = "Replace with new kotlin-ext function",
    replaceWith = ReplaceWith("merge()", imports = ["import fe.std.iterable"])
)
public fun <K, V> Iterable<Map<K, V>>.merge(
    destination: MutableMap<K, V> = LinkedHashMap(),
): MutableMap<K, V> {
    for (element in this) {
        for ((key, value) in element) {
            destination[key] = value
        }
    }

    return destination
}
