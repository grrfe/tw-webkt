package fe.twwebkt.api

@JvmInline
value class VariablesParameter(override val map: Map<String, Any?>) : Parameter {
    constructor(vararg values: Pair<String, Any?>) : this(values.toMap())

    override val name: String
        get() = "variables"

    companion object {
        val Empty = VariablesParameter()
    }
}
