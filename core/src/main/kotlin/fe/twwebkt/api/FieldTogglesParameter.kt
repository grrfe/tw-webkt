package fe.twwebkt.api

@JvmInline
value class FieldTogglesParameter(override val map: Map<String, Any?>) : Parameter {
    constructor(vararg values: Pair<String, Any?>) : this(values.toMap())

    override val name: String
        get() = "fieldToggles"

    companion object {
        val Empty = FieldTogglesParameter()
    }
}
