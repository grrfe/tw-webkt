package fe.twwebkt.model.timeline

import com.google.gson.JsonObject
import fe.gson.extension.json.array.elementsOrNull
import fe.gson.extension.json.`object`.asArrayOrNull
import fe.gson.extension.json.`object`.asObjectOrNull
import fe.gson.extension.json.`object`.asStringOrNull

sealed class TimelineInstructionType(val type: String) {
    abstract fun unwrap(wrapper: JsonObject): List<Pair<String, JsonObject>>?

    companion object {
        private val types = arrayOf(AddEntries, AddToModule)

        fun find(type: String?): TimelineInstructionType? {
            if (type == null) return null
            return types.firstOrNull { it.type == type }
        }
    }
}

data object AddEntries : TimelineInstructionType("TimelineAddEntries") {
    override fun unwrap(wrapper: JsonObject): List<Pair<String, JsonObject>>? {
        return wrapper
            .asArrayOrNull("entries")
            ?.elementsOrNull<JsonObject>()
            ?.mapNotNull {
                val entryId = it?.asStringOrNull("entryId") ?: return@mapNotNull null
                val content = it.asObjectOrNull("content") ?: return@mapNotNull null

                entryId to content
            }
    }
}

data object AddToModule : TimelineInstructionType("TimelineAddToModule") {
    override fun unwrap(wrapper: JsonObject): List<Pair<String, JsonObject>>? {
        return wrapper
            .asArrayOrNull("moduleItems")
            ?.elementsOrNull<JsonObject>()
            ?.mapNotNull {
                val entryId = it?.asStringOrNull("entryId") ?: return@mapNotNull null
                val content = it.asObjectOrNull("item") ?: return@mapNotNull null

                entryId to content
            }
    }
}
