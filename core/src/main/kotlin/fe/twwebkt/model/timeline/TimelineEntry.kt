package fe.twwebkt.model.timeline

import fe.twwebkt.model.tweet.TweetDetailInfo
import fe.twwebkt.model.user.TwitterUser


sealed interface TimelineEntry {
    data class User(val id: String, val user: TwitterUser?) : TimelineEntry
    data class Tweet(val id: String, val tweet: TweetDetailInfo?) : TimelineEntry
    data class ConversationThread(val id: String, val tweets: List<TweetDetailInfo>?) : TimelineEntry
}

sealed class TimelineCursor(val cursor: String?) : TimelineEntry {
    class Top(value: String?) : TimelineCursor(value)
    class Bottom(value: String?) : TimelineCursor(value)
    class MoreThreads(value: String?) : TimelineCursor(value)
}
