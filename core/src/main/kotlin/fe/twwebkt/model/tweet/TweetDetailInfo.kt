package fe.twwebkt.model.tweet

import fe.twwebkt.model.user.TwitterUser
import java.time.LocalDateTime

data class TweetDetailInfo(
    val tweetId: String,
    val createdAt: LocalDateTime,
    val user: TwitterUser? = null,
    val source: PostSource = PostSource.Unknown,
    val conversationId: String?,
    val replyToId: String?,
    val favorites: Long?,
    val retweets: Long?,
    val views: Long?,
    val text: String?,
    val media: List<String>?,
    val retweetedTweet: TweetDetailInfo? = null,
    val mediaOnlyTweet: Boolean = false,
    val tweetTextSingleUrl: Boolean = false,
)

enum class PostSource {
    iPhone,
    iPad,
    Android,
    Web,
    Unknown
}
