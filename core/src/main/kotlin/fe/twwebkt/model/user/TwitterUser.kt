package fe.twwebkt.model.user

import java.time.LocalDateTime

data class TwitterUser(
    val userId: String,
    val name: String,
    val screenName: String,
    val description: String,
    val createdAt: LocalDateTime,
    val location: String?,
    val mediaCount: Long?,
    val statusCount: Long?,
    val followers: Long?,
    val following: Long?,
)
