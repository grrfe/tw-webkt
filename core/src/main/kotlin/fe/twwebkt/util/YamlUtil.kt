package fe.twwebkt.util

import com.charleskorn.kaml.Yaml
import com.charleskorn.kaml.decodeFromStream
import com.charleskorn.kaml.encodeToStream
import java.nio.file.Path
import kotlin.io.path.inputStream
import kotlin.io.path.outputStream

inline fun <reified T> Path.decodeYaml(yaml: Yaml = Yaml.default): T {
    return inputStream().use {
        yaml.decodeFromStream<T>(it)
    }
}

inline fun <reified T> Path.encodeYaml(
    value: T,
    yaml: Yaml = Yaml.default,
) {
    outputStream().use {
        yaml.encodeToStream<T>(value, it)
    }
}
