package fe.twwebkt.util

import java.util.*
import kotlin.random.asJavaRandom


@OptIn(ExperimentalStdlibApi::class)
fun randomHexString(length: Int, random: Random = kotlin.random.Random.asJavaRandom()): String {
    return ByteArray(length / 2).apply { random.nextBytes(this) }.toHexString()
}
