package fe.twwebkt.runnable

import fe.httpkt.clazz.ClassRequest
import fe.std.result.IResult
import fe.std.result.tryCatch
import fe.twwebkt.android.TwitterSessionHandle

interface TwitterRunnable<Result> {
    fun run(handle: TwitterSessionHandle): Result

    fun <R, E : Any> send(handle: TwitterSessionHandle, request: ClassRequest<R, E>): IResult<R> {
        return tryCatch {
            handle.request.send(request)
        }
    }
}
