package fe.twwebkt.runnable

import com.google.gson.JsonObject
import fe.gson.extension.io.toJson
import fe.httpkt.interceptor.IndexedResponseInterceptor
import fe.httpkt.interceptor.ResponseInterceptor
import fe.std.javatime.time.ISO8601DateTimeFormatter
import java.io.File
import java.time.LocalDateTime

class ResultStorage(
    val interceptor: ResponseInterceptor<JsonObject>,
    val resultFile: File,
) {
    fun saveAll(result: Any) {
        resultFile.toJson(result)
    }
}

fun createStorage(directory: File): ResultStorage {
    val now = LocalDateTime.now()
    val formatted = ISO8601DateTimeFormatter.DefaultFormat.format(now)
    val workDir = File(directory, formatted)
    workDir.mkdirs()

    val responseFile = workDir.resolve(".responses")
    responseFile.mkdirs()

    val interceptor = IndexedResponseInterceptor<JsonObject> { idx, response ->
        responseFile.resolve("${idx}.json").toJson(response)
    }

    return ResultStorage(interceptor, workDir.resolve("result.json"))
}
