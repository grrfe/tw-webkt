package fe.twwebkt.runnable

import fe.httpkt.Request
import fe.httpkt.clazz.ClassRequest
import fe.twwebkt.model.timeline.TimelineCursor
import fe.twwebkt.model.timeline.TimelineEntry
import fe.twwebkt.task.error.RequestTaskError
import mu.two.KLogger
import kotlin.time.Duration
import kotlin.time.Duration.Companion.seconds


class TwitterCursorIterator(
    private val logger: KLogger,
    private val request: Request,
) {
    fun <Error : Any> iterate(
        sleep: Duration = 5.seconds,
        nextRequest: (Int, String?) -> ClassRequest<List<TimelineEntry>, Error>,
        onError: (Any) -> Unit,
    ) = sequence<TimelineEntry> {
        var pageId = 0
        var cursor: String? = null
        try {
            do {
                val classRequest = nextRequest(pageId, cursor)

                val (nextCursor, entries) = send(classRequest)
                logger.info("cursor=${cursor}, nextCursor=$nextCursor")
                cursor = getNextCursor(cursor, nextCursor, entries)

                yieldAll(entries)

                pageId++
                Thread.sleep(sleep.inWholeMilliseconds)
            } while (cursor != null)
        } catch (e: ClassRequest.ClassRequestFailedException) {
            when (e.error) {
                is RequestTaskError.JsonErrors -> {
                    for (jsonError in (e.error as RequestTaskError.JsonErrors).errors) {
                        logger.error("JsonErrors: $jsonError")
                    }
                }

                is RequestTaskError.RateLimited -> {
                    logger.error("RateLimited: ${e.error}")
                }

                is RequestTaskError.Unknown -> {
                    val unknownError = e.error as RequestTaskError.Unknown
                    logger.error("Unknown error: $unknownError")
                }
            }

            onError(e.error)
        }
    }

    private fun <Error : Any> send(
        classRequest: ClassRequest<List<TimelineEntry>, Error>,
    ): Pair<String?, MutableList<TimelineEntry>> {
        val response = request.send(classRequest)

        val entries = mutableListOf<TimelineEntry>()
        var cursor: String? = null
        for (entry in response) {
            if (entry !is TimelineCursor.Bottom) {
                entries.add(entry)
                continue
            }

            cursor = entry.cursor
        }

        return cursor to entries
    }

    private fun getNextCursor(cursor: String?, nextCursor: String?, entries: List<TimelineEntry>): String? {
        return when {
            entries.size <= 2 && entries.all { it is TimelineCursor } -> null
            cursor == null || cursor != nextCursor -> nextCursor
            else -> null
        }
    }
}




