package fe.twwebkt.extension

import com.google.gson.JsonObject
import fe.gson.extension.json.`object`.asObjectOrNull

fun JsonObject.nestedUnwrap(vararg keys: String, idx: Int = 0): JsonObject? {
    val key = keys.getOrNull(idx) ?: return this
    val unwrapped = asObjectOrNull(key) ?: return null
    return unwrapped.nestedUnwrap(*keys, idx = idx + 1)
}

fun JsonObject.unwrapAnyOf(vararg keys: String): JsonObject? {
    for (key in keys) {
        if (has(key)) return asObjectOrNull(key)
    }

    return null
}
