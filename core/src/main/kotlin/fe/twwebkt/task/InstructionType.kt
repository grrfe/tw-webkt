package fe.twwebkt.task

import com.google.gson.JsonArray
import com.google.gson.JsonObject
import fe.gson.extension.json.`object`.asArrayOrNull
import fe.twwebkt.extension.nestedUnwrap


sealed class InstructionType(vararg val keys: String) {
    data object ThreadedConversation : InstructionType("data", "threaded_conversation_with_injections_v2")
    data object BookmarkTimeline : InstructionType("data", "bookmark_timeline_v2", "timeline")
    data object UserTimeline : InstructionType("data", "user", "result", "timeline", "timeline")
    data object UserTimelineV2 : InstructionType("data", "user", "result", "timeline_v2", "timeline")
}

fun InstructionType.unwrapInstructions(response: JsonObject): JsonArray? {
    return response.nestedUnwrap(*keys)?.asArrayOrNull("instructions")
}
