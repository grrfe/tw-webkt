package fe.twwebkt.task.error

sealed interface RequestTaskError {
    data object RateLimited : RequestTaskError
    data class JsonErrors(val errors: List<JsonError>) : RequestTaskError
    data class Unknown(val status: Int, val contentType: String, val response: String) : RequestTaskError
}

sealed interface JsonError {
    data class BadRequestError(val message: String?) : JsonError
}
