package fe.twwebkt.task.error

import com.google.gson.JsonObject
import fe.gson.extension.json.array.elementsOrNull
import fe.gson.extension.json.`object`.asArrayOrNull
import fe.gson.extension.json.`object`.asIntOrNull
import fe.gson.extension.json.`object`.asStringOrNull
import fe.gson.util.Json
import fe.httpkt.HttpContentType
import fe.httpkt.HttpStatus
import fe.httpkt.isMatch

object RequestTaskErrorHandler {
    fun handleError(status: Int, contentType: String, response: String): RequestTaskError {
        if (HttpStatus.TooManyRequests.code == status) {
            return RequestTaskError.RateLimited
        }

        if (HttpContentType.ApplicationJson.isMatch(contentType)) {
            val errors = Json.parseJsonOrNull<JsonObject>(response)
                ?.asArrayOrNull("errors")
                ?.elementsOrNull<JsonObject>()
                ?.filterNotNull()
                ?.mapNotNull { handleJsonError(it) }

            if (errors != null) {
                return RequestTaskError.JsonErrors(errors)
            }
        }

        return RequestTaskError.Unknown(status, contentType, response)
    }

    fun handleJsonError(error: JsonObject): JsonError? {
        val code = error.asIntOrNull("code")
        val message = error.asStringOrNull("message")

        return when (code) {
            336 -> JsonError.BadRequestError(message)
            else -> null
        }
    }
}
