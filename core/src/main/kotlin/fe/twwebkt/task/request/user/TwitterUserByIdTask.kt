package fe.twwebkt.task.request.user

import com.google.gson.JsonObject
import fe.httpkt.json.readToJsonElement
import fe.twwebkt.TwitterEndpoint
import fe.twwebkt.api.FeaturesParameter
import fe.twwebkt.api.TwitterGetEndpoint
import fe.twwebkt.api.VariablesParameter
import fe.twwebkt.model.user.TwitterUser
import fe.twwebkt.serializer.user.TwitterUserSerializer
import java.net.HttpURLConnection

class TwitterUserByIdTask(
    private val twitterUserSerializer: TwitterUserSerializer,
    userId: String
) : TwitterGetEndpoint<TwitterUser?>(
    endpoint = TwitterEndpoint.ShowUser,
    features = FeaturesParameter.Default,
    variables = VariablesParameter(
        "user_id" to userId,
    )
) {
    override fun handleSuccess(con: HttpURLConnection): TwitterUser? {
        val response = con.readToJsonElement<JsonObject>()
        return twitterUserSerializer.handleUser(response)
    }
}
