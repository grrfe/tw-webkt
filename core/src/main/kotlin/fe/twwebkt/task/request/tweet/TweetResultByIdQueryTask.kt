package fe.twwebkt.task.request.tweet

import com.google.gson.JsonObject
import fe.httpkt.interceptor.ResponseInterceptor
import fe.httpkt.json.readToJsonElement
import fe.twwebkt.TwitterEndpoint
import fe.twwebkt.api.FeaturesParameter
import fe.twwebkt.api.TwitterGetEndpoint
import fe.twwebkt.api.VariablesParameter
import fe.twwebkt.model.tweet.TweetDetailInfo
import fe.twwebkt.serializer.tweet.TweetDetailInfoSerializer
import java.net.HttpURLConnection


class TweetResultByIdQueryTask(
    private val tweetDetailInfoSerializer: TweetDetailInfoSerializer,
    private val interceptor: ResponseInterceptor<JsonObject>? = null,
    tweetId: String,
) : TwitterGetEndpoint<TweetDetailInfo?>(
    endpoint = TwitterEndpoint.TweetResultByIdQuery,
    features = FeaturesParameter.Default,
    variables = VariablesParameter(
        "rest_id" to tweetId,
        "includeTweetImpression" to true,
        "includeHasBirdwatchNotes" to false,
        "includeEditPerspective" to false,
        "includeEditControl" to true,
        "includeCommunityTweetRelationship" to true
    ),
) {
    override fun handleSuccess(con: HttpURLConnection): TweetDetailInfo? {
        val response = con.readToJsonElement<JsonObject>()
        interceptor?.onSuccess(response)

        return tweetDetailInfoSerializer.handleTweet(response)
    }
}
