package fe.twwebkt.task.request.follow

import fe.httpkt.Request
import fe.httpkt.body.FormUrlEncodedBody
import fe.httpkt.util.headersOf
import fe.twwebkt.TwitterEndpoint
import fe.twwebkt.api.TwitterBodyEndpoint
import java.net.HttpURLConnection


class TwitterFollowTask(userId: String) : TwitterBodyEndpoint<TwitterEndpoint.CreateFriendships, Unit>(
    method = Request.HttpMethod.Post,
    endpoint = TwitterEndpoint.CreateFriendships,
    body = {
        FormUrlEncodedBody(
            "include_profile_interstitial_type" to "1",
            "include_blocking" to "1",
            "include_blocked_by" to "1",
            "include_followed_by" to "1",
            "include_want_retweets" to "1",
            "include_mute_edge" to "1",
            "include_can_dm" to "1",
            "include_can_media_tag" to "1",
            "include_ext_has_nft_avatar" to "1",
            "include_ext_has_nft_avatar" to "1",
            "include_ext_verified_type" to "1",
            "include_ext_profile_image_shape" to "1",
            "skip_status" to "1",
            "id" to userId
        )
    },
    data = TwitterEndpoint.TwitterOAuth2SessionType
) {
    override fun handleSuccess(con: HttpURLConnection) {}
}
