package fe.twwebkt.task.request.login

import com.google.gson.JsonObject
import fe.gson.dsl.jsonObject
import fe.gson.extension.json.element.objectOrNull
import fe.gson.extension.json.`object`.asStringOrNull
import fe.httpkt.Request
import fe.httpkt.Session
import fe.httpkt.auth.AuthType
import fe.httpkt.auth.createHeader
import fe.httpkt.body.FormUrlEncodedBody
import fe.httpkt.data.buildGetString
import fe.httpkt.ext.findHeader
import fe.httpkt.flow.DataFlowRequest
import fe.httpkt.json.JsonBody
import fe.httpkt.json.readToJson
import fe.httpkt.json.readToJsonElement
import fe.twwebkt.TwitterEndpoint
import fe.twwebkt.android.TwitterContext
import fe.twwebkt.android.device.KnownDeviceSession
import fe.twwebkt.android.util.TwitterHeaders
import java.net.HttpURLConnection

class TwitterAndroidLoginFlow(
    private val twitterInstance: TwitterContext,
) : DataFlowRequest<TwitterAccount, KnownDeviceSession, String, Session> {

    override fun execute(data: TwitterAccount, request: Session): KnownDeviceSession {
        request.update { addHeaders(twitterInstance.headers) }

        val accessToken = requestToken(request) ?: fail("Couldn't fetch access token")
        request.update(data = AuthType.Bearer.createHeader(accessToken))

        val guestToken = activateGuest(request) ?: fail("Failed to active guest")
        val guestTokenHeaders = twitterInstance.createGuestLoginHeaders(guestToken)

        request.update { addHeaders(guestTokenHeaders) }

        val initResponse = initLogin(request)
        val att = initResponse.findHeader(TwitterHeaders.APP_TRACKING_TRANSPARENCY) ?: fail("No att header")
        val flowToken = initResponse.readFlowToken() ?: fail("Failed to read initial flowToken")

        request.update { addHeader(att) }

        return LoginStateMachine(data).login(request, flowToken)
    }

    private fun HttpURLConnection.readFlowToken(): String? {
        return readToJsonElement<JsonObject>().also { println(it) }.asStringOrNull("flow_token")
    }

    private fun requestToken(request: Session): String? {
        return request.request(
            Request.HttpMethod.Post,
            TwitterEndpoint.BaseApiUrl.atEndpoint(TwitterEndpoint.Oauth2Token),
            data = AuthType.Basic.createHeader(twitterInstance.twitterVersion.basicAuth),
            body = FormUrlEncodedBody("grant_type" to "client_credentials")
        ).readToJson().objectOrNull()?.asStringOrNull("access_token")
    }

    private fun activateGuest(request: Session): String? {
        return request.request(
            Request.HttpMethod.Post,
            TwitterEndpoint.BaseApiUrl.atEndpoint(TwitterEndpoint.Activate),
        ).readToJson().objectOrNull()?.asStringOrNull("guest_token")
    }

    private fun initLogin(request: Session): HttpURLConnection {
        val simCountryCode = twitterInstance.locale.simCountry
        val getParams = buildGetString(
            "flow_name" to "login",
            "api_version" to "1",
            "known_device_token" to "",
            "sim_country_code" to simCountryCode
        )

        return request.request(
            Request.HttpMethod.Post,
            TwitterEndpoint.BaseApiUrl.atEndpoint(TwitterEndpoint.Onboarding) + getParams,
            body = JsonBody(jsonObject {
                "input_flow_data" += {
                    "country_code" += null
                    "flow_context" += {
                        "start_location" += {
                            "location" += "deeplink"
                        }
                        "referrer_context" += {
                            "referral_details" += "utm_source=google-play&utm_medium=organic"
                            "referrer_url" += ""
                        }
                    }
                    "requested_variant" += null
                    "target_user_id" += 0
                }
                "subtask_versions" += LoginSubtaskVersion.allVersions
            })
        )
    }
}

