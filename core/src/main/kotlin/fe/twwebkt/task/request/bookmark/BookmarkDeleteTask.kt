package fe.twwebkt.task.request.bookmark

import com.google.gson.JsonObject
import fe.gson.dsl.jsonObject
import fe.gson.extension.json.`object`.asObjectOrNull
import fe.gson.extension.json.`object`.asStringOrNull
import fe.httpkt.Request
import fe.httpkt.json.readToJsonElement
import fe.twwebkt.TwitterEndpoint
import fe.twwebkt.api.TwitterBodyEndpoint
import java.net.HttpURLConnection

class BookmarkDeleteTask(tweetId: String) : TwitterBodyEndpoint<TwitterEndpoint.BookmarkDelete, Boolean>(
    method = Request.HttpMethod.Post,
    endpoint = TwitterEndpoint.BookmarkDelete,
    body = {
        jsonBody {
            "variables" += jsonObject {
                "tweet_id" += tweetId
            }
        }
    },
    data = TwitterEndpoint.TwitterOAuth2SessionType
) {
    override fun handleSuccess(con: HttpURLConnection): Boolean {
        val response = con.readToJsonElement<JsonObject>()

        return response.asObjectOrNull("data")?.asStringOrNull("tweet_bookmark_delete") == "Done"
    }
}
