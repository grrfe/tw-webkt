package fe.twwebkt.task.request.account

import com.google.gson.JsonArray
import com.google.gson.JsonObject
import fe.gson.extension.json.`object`.asArrayOrNull
import fe.httpkt.json.readToJsonElement
import fe.twwebkt.TwitterEndpoint
import fe.twwebkt.api.TwitterGetEndpoint
import fe.twwebkt.extension.nestedUnwrap
import java.net.HttpURLConnection

object TwitterGetUserClaims :  TwitterGetEndpoint<JsonArray?>(
    endpoint = TwitterEndpoint.GetUserClaims,
    data = TwitterEndpoint.TwitterOAuth2SessionType
) {
    override fun handleSuccess(con: HttpURLConnection): JsonArray? {
        val response = con.readToJsonElement<JsonObject>()

        return response
            .nestedUnwrap("data", "viewer_v2")
            ?.asArrayOrNull("claims")
    }
}
