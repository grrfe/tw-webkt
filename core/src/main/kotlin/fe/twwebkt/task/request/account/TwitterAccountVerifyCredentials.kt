package fe.twwebkt.task.request.account

import fe.httpkt.ext.readToString
import fe.twwebkt.TwitterEndpoint
import fe.twwebkt.api.TwitterGetEndpoint
import java.net.HttpURLConnection

object TwitterAccountVerifyCredentials : TwitterGetEndpoint<Unit>(
    endpoint = TwitterEndpoint.VerifyCredentials
) {
    override fun handleSuccess(con: HttpURLConnection) {
        println("Success ${con.readToString()}")
    }
}
