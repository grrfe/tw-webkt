package fe.twwebkt.task.request.login

import com.eatthepath.otp.TimeBasedOneTimePasswordGenerator
import com.google.gson.JsonObject
import fe.gson.dsl.jsonObject
import fe.gson.util.jsonArrayWithSingleJsonObject
import fe.httpkt.Request
import fe.httpkt.Session
import fe.httpkt.json.JsonBody
import fe.twwebkt.TwitterEndpoint
import java.net.HttpURLConnection
import java.time.Instant

interface LoginSubtask {
    fun toPayload(): JsonObject
}

sealed class LoginSubtaskData(
    private val taskId: String,
    private val key: String,
    private val link: String = "next_link",
) : LoginSubtask {

    abstract override fun toPayload(): JsonObject

    private fun createPayload(flowToken: String): JsonObject {
        return jsonObject {
            "flow_token" += flowToken
            "subtask_inputs" += jsonArrayWithSingleJsonObject {
                "subtask_id" += taskId
                key += jsonObject(toPayload()) {
                    "link" += link
                }
            }
            "subtask_versions" += LoginSubtaskVersion.allVersions
        }
    }

    fun send(session: Session, flowToken: String): HttpURLConnection {
        return session.request(
            Request.HttpMethod.Post,
            TwitterEndpoint.BaseApiUrl.atEndpoint(TwitterEndpoint.Onboarding),
            JsonBody(createPayload(flowToken))
        )
    }
}

class UsernameAndroid(private val username: String) : LoginSubtaskData(
    taskId = "LoginEnterUserIdentifier",
    key = "enter_text"
) {
    override fun toPayload(): JsonObject {
        return jsonObject {
            "challenge_response" += null
            "suggestion_id" += null
            "text" += username
        }
    }
}

class Password(private val password: String) : LoginSubtaskData(
    taskId = "LoginEnterPassword",
    key = "enter_password"
) {
    override fun toPayload(): JsonObject {
        return jsonObject { "password" += password }
    }
}

data object DuplicationCheck : LoginSubtaskData(
    "AccountDuplicationCheck",
    "check_logged_in_account",
    "AccountDuplicationCheck_false",
) {
    override fun toPayload(): JsonObject {
        return JsonObject()
    }
}


class AlternativeIdentifier(private val email: String) : LoginSubtaskData(
    taskId = "LoginEnterAlternateIdentifierSubtask",
    key = "enter_text"
) {
    override fun toPayload(): JsonObject {
        return jsonObject {
            "challenge_response" += null
            "suggestion_id" += null
            "text" += email
        }
    }
}

class TwoFactorChallenge(
    private val totp: TOTP,
    private val generator: TimeBasedOneTimePasswordGenerator = TimeBasedOneTimePasswordGenerator(),
) : LoginSubtaskData(
    taskId = "LoginTwoFactorAuthChallenge",
    key = "enter_text"
) {
    override fun toPayload(): JsonObject {
        return jsonObject {
            "challenge_response" += null
            "suggestion_id" += null
            "text" += generator.generateOneTimePasswordString(totp.key, Instant.now())
        }
    }
}
