package fe.twwebkt.task.request.tweet

import com.google.gson.JsonObject
import fe.httpkt.interceptor.ResponseInterceptor
import fe.httpkt.json.readToJsonElement
import fe.twwebkt.TwitterEndpoint
import fe.twwebkt.api.FeaturesParameter
import fe.twwebkt.api.TwitterGetEndpoint
import fe.twwebkt.api.VariablesParameter
import fe.twwebkt.model.timeline.TimelineEntry
import fe.twwebkt.serializer.timeline.TimelineSerializer
import fe.twwebkt.task.InstructionType
import fe.twwebkt.task.unwrapInstructions
import java.net.HttpURLConnection


class TweetConversationTask(
    private val timelineSerializer: TimelineSerializer,
    private val interceptor: ResponseInterceptor<JsonObject>? = null,
    val tweetId: String,
    cursor: String? = null,
) : TwitterGetEndpoint<List<TimelineEntry>>(
    endpoint = TwitterEndpoint.TweetConversation,
    variables = VariablesParameter(
        "focalTweetId" to tweetId,
        "cursor" to cursor,
        "referrer" to "messages",
        "with_rux_injections" to false,
        "rankingMode" to "Relevance",
        "includePromotedContent" to true,
        "withCommunity" to true,
        "withQuickPromoteEligibilityTweetFields" to true,
        "withBirdwatchNotes" to true,
        "withVoice" to true
    ),
    features = FeaturesParameter.Default
) {
    override fun handleSuccess(con: HttpURLConnection): List<TimelineEntry> {
        val response = con.readToJsonElement<JsonObject>()
        interceptor?.onSuccess(response)

        val instructions = InstructionType.ThreadedConversation.unwrapInstructions(response)
        val entries = timelineSerializer.deserialize(instructions)
        return entries ?: emptyList()
    }
}
