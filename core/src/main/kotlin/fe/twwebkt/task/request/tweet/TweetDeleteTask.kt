package fe.twwebkt.task.request.tweet

import fe.gson.dsl.jsonObject
import fe.httpkt.Request
import fe.httpkt.ext.readToString
import fe.httpkt.util.headersOf
import fe.twwebkt.TwitterEndpoint
import fe.twwebkt.api.TwitterBodyEndpoint
import java.net.HttpURLConnection


class TweetDeleteTask(tweetId: String) : TwitterBodyEndpoint<TwitterEndpoint.TweetDelete, Unit>(
    method = Request.HttpMethod.Post,
    endpoint = TwitterEndpoint.TweetDelete,
    body = {
        jsonBody {
            "variables" += jsonObject {
                "tweet_id" += tweetId
                "dark_request" += false
            }
        }
    },
    data = TwitterEndpoint.TwitterOAuth2SessionType
) {
    override fun handleSuccess(con: HttpURLConnection) {}
}
