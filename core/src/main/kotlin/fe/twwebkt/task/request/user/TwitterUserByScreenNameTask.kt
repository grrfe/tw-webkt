package fe.twwebkt.task.request.user

import com.google.gson.JsonObject
import fe.httpkt.json.readToJsonElement
import fe.twwebkt.TwitterEndpoint
import fe.twwebkt.api.FeaturesParameter
import fe.twwebkt.api.FieldTogglesParameter
import fe.twwebkt.api.TwitterGetEndpoint
import fe.twwebkt.api.VariablesParameter
import fe.twwebkt.model.user.TwitterUser
import fe.twwebkt.serializer.user.TwitterUserSerializer
import java.net.HttpURLConnection

class TwitterUserByScreenNameTask(
    screenName: String,
    private val twitterUserSerializer: TwitterUserSerializer,
) : TwitterGetEndpoint<TwitterUser?>(
    endpoint = TwitterEndpoint.UserByScreenName,
    features = FeaturesParameter.Default,
    fieldToggles = FieldTogglesParameter("withAuxiliaryUserLabels" to false),
    variables = VariablesParameter(
        "screen_name" to screenName,
        "withSafetyModeUserFields" to true,
    ),
    data = TwitterEndpoint.TwitterOAuth2SessionType
) {
    override fun handleSuccess(con: HttpURLConnection): TwitterUser? {
        val response = con.readToJsonElement<JsonObject>()
        return twitterUserSerializer.handleUser(response)
    }
}
