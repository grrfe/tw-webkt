package fe.twwebkt.task.request.following

import com.google.gson.JsonObject
import fe.httpkt.interceptor.ResponseInterceptor
import fe.httpkt.json.readToJsonElement
import fe.twwebkt.TwitterEndpoint
import fe.twwebkt.api.FeaturesParameter
import fe.twwebkt.api.TwitterGetEndpoint
import fe.twwebkt.api.VariablesParameter
import fe.twwebkt.model.timeline.TimelineEntry
import fe.twwebkt.serializer.timeline.TimelineSerializer
import fe.twwebkt.task.InstructionType
import fe.twwebkt.task.unwrapInstructions
import java.net.HttpURLConnection

class TwitterFollowingTask(
    private val timelineSerializer: TimelineSerializer,
    private val interceptor: ResponseInterceptor<JsonObject>? = null,
    userId: String,
    count: Int = 20,
    cursor: String? = null,
) : TwitterGetEndpoint<List<TimelineEntry>>(
    endpoint = TwitterEndpoint.Following,
    features = FeaturesParameter.Default,
    variables = VariablesParameter(
        "userId" to userId,
        "count" to count,
        "cursor" to cursor,
        "includePromotedContent" to false
    ),
    data = TwitterEndpoint.TwitterOAuth2SessionType
) {
    override fun handleSuccess(con: HttpURLConnection): List<TimelineEntry> {
        val response = con.readToJsonElement<JsonObject>()
        interceptor?.onSuccess(response)

        val instructions = InstructionType.UserTimeline.unwrapInstructions(response)
        val entries = timelineSerializer.deserialize(instructions)
        return entries ?: emptyList()
    }
}
