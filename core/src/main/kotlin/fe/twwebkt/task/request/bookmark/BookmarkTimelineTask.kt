package fe.twwebkt.task.request.bookmark

import com.google.gson.JsonObject
import fe.httpkt.interceptor.ResponseInterceptor
import fe.httpkt.json.readToJsonElement
import fe.twwebkt.TwitterEndpoint
import fe.twwebkt.api.FeaturesParameter
import fe.twwebkt.api.TwitterGetEndpoint
import fe.twwebkt.api.VariablesParameter
import fe.twwebkt.model.timeline.TimelineEntry
import fe.twwebkt.serializer.timeline.TimelineSerializer
import fe.twwebkt.task.InstructionType
import fe.twwebkt.task.unwrapInstructions
import java.net.HttpURLConnection

class BookmarkTimelineTask(
    private val timelineSerializer: TimelineSerializer,
    private val interceptor: ResponseInterceptor<JsonObject>? = null,
    count: Int = 20,
    cursor: String? = null,
) : TwitterGetEndpoint<List<TimelineEntry>>(
    endpoint = TwitterEndpoint.Bookmarks,
    features = FeaturesParameter.Default,
    variables = VariablesParameter(
        "count" to count,
        "cursor" to cursor,
    ),
) {
    override fun handleSuccess(con: HttpURLConnection): List<TimelineEntry> {
        val response = con.readToJsonElement<JsonObject>()
        interceptor?.onSuccess(response)

        val instructions = InstructionType.BookmarkTimeline.unwrapInstructions(response)
        val entries = timelineSerializer.deserialize(instructions)
        return entries ?: emptyList()
    }
}


