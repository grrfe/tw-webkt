package fe.twwebkt.task.request.login

import com.eatthepath.otp.TimeBasedOneTimePasswordGenerator.TOTP_ALGORITHM_HMAC_SHA1
import io.matthewnelson.encoding.base32.Base32
import io.matthewnelson.encoding.core.Decoder.Companion.decodeToByteArray
import kotlinx.serialization.Serializable
import kotlinx.serialization.Transient
import javax.crypto.spec.SecretKeySpec

@Serializable
data class TwitterAccount(
    val email: String,
    val username: String,
    val password: String,
    val totp: TOTP? = null,
)

@Serializable
data class TOTP(val algorithm: String = TOTP_ALGORITHM_HMAC_SHA1, private val secret: String) {
    @Transient
    val key = SecretKeySpec(secret.decodeToByteArray(Base32.Default), algorithm)
}
