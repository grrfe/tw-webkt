package fe.twwebkt.task.request.user

import com.google.gson.JsonObject
import fe.httpkt.interceptor.ResponseInterceptor
import fe.httpkt.json.readToJsonElement
import fe.twwebkt.TwitterEndpoint
import fe.twwebkt.api.FeaturesParameter
import fe.twwebkt.api.FieldTogglesParameter
import fe.twwebkt.api.TwitterGetEndpoint
import fe.twwebkt.api.VariablesParameter
import fe.twwebkt.model.timeline.TimelineEntry
import fe.twwebkt.serializer.timeline.TimelineSerializer
import fe.twwebkt.task.InstructionType
import fe.twwebkt.task.unwrapInstructions
import java.net.HttpURLConnection

class TwitterUserMediaTask(
    private val timelineSerializer: TimelineSerializer,
    private val interceptor: ResponseInterceptor<JsonObject>? = null,
    userId: String,
    count: Int = 20,
    cursor: String? = null,
) : TwitterGetEndpoint<List<TimelineEntry>>(
    endpoint = TwitterEndpoint.UserMedia,
    features = FeaturesParameter.Default,
    fieldToggles = FieldTogglesParameter(
        "withArticlePlainText" to false,
    ),
    variables = VariablesParameter(
        "userId" to userId,
        "count" to count,
        "cursor" to cursor,
        "includePromotedContent" to false,
        "withBirdwatchNotes" to false,
        "withClientEventToken" to false,
        "withVoice" to true,
        "withV2Timeline" to true
    ),
    data = TwitterEndpoint.TwitterOAuth2SessionType
) {
    override fun handleSuccess(con: HttpURLConnection): List<TimelineEntry> {
        val response = con.readToJsonElement<JsonObject>()
        interceptor?.onSuccess(response)

        val instructions = InstructionType.UserTimelineV2.unwrapInstructions(response)
        val entries = timelineSerializer.deserialize(instructions)
        return entries ?: emptyList()
    }
}
