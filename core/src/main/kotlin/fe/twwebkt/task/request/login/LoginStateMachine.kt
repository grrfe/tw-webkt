package fe.twwebkt.task.request.login

import com.google.gson.JsonArray
import com.google.gson.JsonObject
import fe.gson.extension.json.array.elementsOrNull
import fe.gson.extension.json.element.objectOrNull
import fe.gson.extension.json.`object`.asArrayOrNull
import fe.gson.extension.json.`object`.asIntOrNull
import fe.gson.extension.json.`object`.asObjectOrNull
import fe.gson.extension.json.`object`.asStringOrNull
import fe.httpkt.Session
import fe.httpkt.json.readToJsonElement
import fe.twwebkt.android.device.KnownDeviceSession
import mu.two.KotlinLogging


sealed interface LoginState {
    data class TaskState(
        val subtask: LoginSubtaskData,
        val flowToken: String,
        val data: JsonObject? = null,
    ) : LoginState

    data class SuccessState(
        val oauthToken: String,
        val oauthSecret: String,
        val knownDeviceToken: String,
    ) : LoginState
}

sealed interface LoginError : LoginState {
    data object NoFlowTokenError : LoginState
    data object NoSubtaskId : LoginState

    data class LoginError(val errors: Map<Int, String>) : LoginState
    data class TaskError(val subtask: Subtask, val message: String) : LoginState
}

enum class Subtask {
    LoginEnterUserIdentifier,
    LoginEnterAlternateIdentifierSubtask,
    LoginEnterPassword,
    LoginTwoFactorAuthChallenge,
    AccountDuplicationCheck,
    LoginSuccessSubtask,
//    DenyLoginSubtask
}

class LoginStateMachine(private val data: TwitterAccount) {
    private val logger = KotlinLogging.logger(LoginStateMachine::class.simpleName!!)

    fun login(session: Session, flowToken: String): KnownDeviceSession {
        var currentState: LoginState = Subtask.LoginEnterUserIdentifier.nextState(flowToken, null)
        while (currentState is LoginState.TaskState) {
            logger.info { "Sending $currentState" }

            val nextState = currentState.send(session)

            logger.info { "Next state is $nextState" }
            if (nextState is LoginState.SuccessState) {
                return KnownDeviceSession(nextState.oauthToken, nextState.oauthSecret, nextState.knownDeviceToken)
            }

            currentState = nextState
        }

        throw Exception(currentState.toString())
    }

    // {"flow_token": "", status: "success", "subtasks": [{"subtaskId": "LoginSSO", ...}]}
    // {"errors": [{"code": 399, "message": "test"}]}
    private fun LoginState.TaskState.send(session: Session): LoginState {
        val con = subtask.send(session, flowToken)
        val response = con.readToJsonElement<JsonObject>()

        val errors = response.asArrayOrNull("errors")
        if (errors != null) {
            val map = mapErrors(errors)
            return LoginError.LoginError(map)
        }

        val flowToken = response.asStringOrNull("flow_token") ?: return LoginError.NoFlowTokenError
        val subtaskObject = response.asArrayOrNull("subtasks")
            ?.firstOrNull()
            ?.objectOrNull()

        val subtaskId = subtaskObject?.asStringOrNull("subtask_id") ?: return LoginError.NoSubtaskId
        val subtask = enumValueOf<Subtask>(subtaskId)

        return subtask.nextState(flowToken, subtaskObject)
    }

    private fun Subtask.nextState(flowToken: String, nextData: JsonObject?): LoginState {
        logger.info { "Next subtask: $this" }
        return when (this) {
            Subtask.LoginEnterUserIdentifier -> UsernameAndroid(data.username).asTask(flowToken, nextData)
            Subtask.LoginEnterAlternateIdentifierSubtask -> AlternativeIdentifier(data.email).asTask(flowToken, nextData)
            Subtask.LoginEnterPassword -> Password(data.password).asTask(flowToken, nextData)
            Subtask.LoginTwoFactorAuthChallenge -> TwoFactorChallenge(data.totp!!).asTask(flowToken, nextData)
            Subtask.AccountDuplicationCheck -> DuplicationCheck.asTask(flowToken, nextData)
            Subtask.LoginSuccessSubtask -> handleLoginSuccess(this, nextData)
        }
    }

    private fun LoginSubtaskData.asTask(flowToken: String, nextData: JsonObject?): LoginState.TaskState {
        return LoginState.TaskState(this, flowToken, nextData)
    }

    private fun handleLoginSuccess(subtask: Subtask, data: JsonObject?): LoginState {
        val openAccount = data?.asObjectOrNull("open_account")
            ?: return LoginError.TaskError(subtask, "Failed to get 'open_account' object")

        val oauthToken = openAccount.asStringOrNull("oauth_token")
            ?: return LoginError.TaskError(subtask, "Couldn't find oauth token")

        val oauthSecret = openAccount.asStringOrNull("oauth_token_secret")
            ?: return LoginError.TaskError(subtask, "Couldn't find oauth secret")

        val knownDeviceToken = openAccount.asStringOrNull("known_device_token")
            ?: return LoginError.TaskError(subtask, "Couldn't find known device token")

        return LoginState.SuccessState(oauthToken, oauthSecret, knownDeviceToken)
    }

    private fun mapError(obj: JsonObject?): Pair<Int, String>? {
        if (obj == null) return null
        val code = obj.asIntOrNull("code") ?: return null
        val message = obj.asStringOrNull("message") ?: return null

        return code to message
    }

    private fun mapErrors(errors: JsonArray): Map<Int, String> {
        return errors.elementsOrNull<JsonObject>().mapNotNull { mapError(it) }.toMap()
    }
}


