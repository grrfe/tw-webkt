package fe.twwebkt.task.request.login

import fe.gson.dsl.lazyJsonObject

object LoginSubtaskVersion {
    private val one = setOf(
        "standard", "open_home_timeline", "app_locale_update", "enter_date", "deregister_device",
        "single_sign_on", "fetch_persisted_data", "fetch_temporary_password", "menu_dialog", "in_app_notification",
        "typeahead_search", "cta_inline", "js_instrumentation", "alert_dialog_suppress_client_events",
        "privacy_options", "topics_selector", "tweet_selection_urt", "end_flow", "open_external_link", "upload_media",
        "alert_dialog", "passkey", "open_link", "show_code", "update_users", "check_logged_in_account"
    )

    private val two = setOf(
        "one_tap", "web_modal", "sign_up", "select_banner", "web", "open_account", "action_list", "enter_phone",
        "enter_email", "location_permission_prompt"
    )

    private val three = setOf(
        "generic_urt", "email_verification", "enter_username", "user_recommendations_urt",
        "contacts_live_sync_permission_prompt", "wait_spinner", "security_key"
    )

    private val four = setOf("user_recommendations_list", "select_avatar", "notifications_permission_prompt")
    private val five = setOf("enter_password", "sign_up_review", "choice_selection", "phone_verification")
    private val six = setOf("enter_text")
    private val seven = setOf("cta", "settings_list")

    private val versions = arrayOf(one, two, three, four, five, six, seven)

    val allVersions by lazyJsonObject {
        versions.forEachIndexed { index, values ->
            values.forEach { task -> task += (index + 1) }
        }
    }
}
