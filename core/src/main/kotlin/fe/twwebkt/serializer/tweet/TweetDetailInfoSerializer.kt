package fe.twwebkt.serializer.tweet

import com.google.gson.JsonArray
import com.google.gson.JsonObject
import fe.gson.extension.json.array.elementsOrNull
import fe.gson.extension.json.element.intOrNull
import fe.gson.extension.json.`object`.*
import fe.twwebkt.extension.nestedUnwrap
import fe.twwebkt.model.tweet.PostSource
import fe.twwebkt.model.tweet.TweetDetailInfo
import fe.twwebkt.serializer.TwitterDateParser
import fe.twwebkt.serializer.user.TwitterUserSerializer

class TweetDetailInfoSerializer(private val twitterUserSerializer: TwitterUserSerializer) {
    fun handleConversationThread(content: JsonObject): List<TweetDetailInfo>? {
        // TimelineTimelineModule
        return content
            .asArrayOrNull("items")
            ?.elementsOrNull<JsonObject>()
            ?.mapNotNull { it?.asObjectOrNull("item") }
            ?.mapNotNull { handleTweet(it) }
    }

    fun handleTweet(content: JsonObject): TweetDetailInfo? {
        // TimelineTimelineItem
        val result = unwrapTweetResult(content)
        return deserialize(result)
    }

    internal fun unwrapTweetResult(response: JsonObject): JsonObject? {
        if (response.has("data")) {
            return response.nestedUnwrap("data", "tweet_result", "result")
        }

        return response.nestedUnwrap("itemContent", "tweet_results", "result")
    }

    private fun findBestVideo(media: JsonObject): String {
        return media
            .asObjectOrNull("video_info")
            ?.asArrayOrNull("variants")
            ?.elementsOrNull<JsonObject>()
            ?.maxBy { it?.asLongOrNull("bitrate") ?: 0 }
            ?.asStringOrNull("url")!!
    }

    private fun getMediaUrls(extendedEntities: JsonObject): List<String>? {
        return extendedEntities
            .asArrayOrNull("media")
            ?.elementsOrNull<JsonObject>()
            ?.mapNotNull { media ->
                if (media?.asStringOrNull("type") == "video") {
                    return@mapNotNull findBestVideo(media)
                }

                media?.asStringOrNull("media_url_https")
            }
    }

    internal fun unwrapContainer(result: JsonObject?): JsonObject? {
        if (result?.has("legacy") != false || !result.has("tweet")) return result
        return result.asObjectOrNull("tweet")
    }

    private fun trimTweetText(text: String?): String? {
        return text?.replace("\n", " ")?.trim()
    }

    private fun isRetweet(legacyTweet: JsonObject?): Boolean {
        return legacyTweet?.asBooleanOrNull("retweeted") == true || legacyTweet?.has("retweeted_status_result") == true
    }

    internal fun parseSource(source: String): PostSource {
        val textIdx = source.indexOf(">")
        val textEndIdx = source.indexOf("</")

        val title = source.substring(textIdx + 1, textEndIdx)

        return when(title) {
            "Twitter for iPhone" -> PostSource.iPhone
            "Twitter for iPad" -> PostSource.iPad
            "Twitter for Android" -> PostSource.Android
            "Twitter Web App" -> PostSource.Web
            else -> PostSource.Unknown
        }
    }

    private fun isMediaOnlyTweet(textRange: JsonArray?): Boolean {
        if (textRange == null) return false
        if (textRange.size() < 2) return false

        val firstPosition = textRange.firstOrNull()?.intOrNull()
        val lastPosition = textRange.lastOrNull()?.intOrNull()

        return firstPosition == 0 && lastPosition == 0
    }

    private fun isTweetTextSingleUrl(text: String?, urls: List<String>?, mediaUrls: List<String>?): Boolean {
        val url = urls?.singleOrNull() ?: return false
        val mediaUrl = mediaUrls?.singleOrNull() ?: return false

        return text == url || text == mediaUrl
    }

    private fun getUrls(entities: JsonObject): List<String>? {
        return entities
            .asArrayOrNull("urls")
            ?.elementsOrNull<JsonObject>()
            ?.mapNotNull { it?.asStringOrNull("url") }
    }

    private fun deserializeRetweetedTweetOrNull(legacyTweet: JsonObject?): TweetDetailInfo? {
        return legacyTweet
            ?.asObjectOrNull("retweeted_status_result")
            ?.asObjectOrNull("result")
            ?.let { deserialize(it) }
    }

    private fun getId(legacyTweet: JsonObject): String? {
        val id = legacyTweet.asStringOrNull("id_str") ?: legacyTweet.asStringOrNull("conversation_id_str")
        return id
    }

    internal fun deserialize(result: JsonObject?): TweetDetailInfo? {
        val container = unwrapContainer(result)
        val legacyTweet = container?.asObjectOrNull("legacy")

        val idStr = legacyTweet?.let { getId(it) } ?: return null
        val views = container.asObjectOrNull("views")?.asLongOrNull("count")
        val source = container.asStringOrNull("source")?.let { parseSource(it) } ?: PostSource.Unknown

        val createdAt = TwitterDateParser.parseOrNull(legacyTweet.asStringOrNull("created_at"))
//        val userId = legacyTweet.asStringOrNull("user_id_str")
        val conversationId = legacyTweet.asStringOrNull("conversation_id_str")
        val replyToId = legacyTweet.asStringOrNull("in_reply_to_status_id_str")
        val favorites = legacyTweet.asLongOrNull("favorite_count")
        val retweets = legacyTweet.asLongOrNull("retweet_count")
        val text = legacyTweet.asStringOrNull("full_text")?.let { trimTweetText(it) }
        val mediaUrls = legacyTweet.asObjectOrNull("extended_entities")?.let { getMediaUrls(it) }
        val urls = legacyTweet.asObjectOrNull("entities")?.let { getUrls(it) }
        val textRange = legacyTweet.asArrayOrNull("display_text_range")

        val retweetedTweet = deserializeRetweetedTweetOrNull(legacyTweet)

//        tweet_results -> result -> core
        // "core" acts as our itemContent here
        val user = result?.let { twitterUserSerializer.handleUser(it) }

        return TweetDetailInfo(
            idStr, createdAt!!,
            user = user,
            source,
            conversationId, replyToId, favorites, retweets, views, text, mediaUrls,
            retweetedTweet = retweetedTweet,
            isMediaOnlyTweet(textRange),
            isTweetTextSingleUrl(text, urls, mediaUrls)
        )
    }
}
