package fe.twwebkt.serializer

import fe.twwebkt.serializer.timeline.TimelineSerializer
import fe.twwebkt.serializer.tweet.TweetDetailInfoSerializer
import fe.twwebkt.serializer.user.TwitterUserSerializer

object DefaultSerializers {
    val twitterUserSerializer = TwitterUserSerializer()
    val tweetDetailInfoSerializer = TweetDetailInfoSerializer(twitterUserSerializer)

    val timelineSerializer = TimelineSerializer(
        twitterUserSerializer = twitterUserSerializer,
        tweetDetailInfoSerializer = tweetDetailInfoSerializer,
    )
}
