package fe.twwebkt.serializer.timeline

import com.google.gson.JsonArray
import com.google.gson.JsonObject
import fe.extension.logger
import fe.gson.extension.json.element.objectOrNull
import fe.gson.extension.json.`object`.asObjectOrNull
import fe.gson.extension.json.`object`.asStringOrNull
import fe.twwebkt.model.timeline.TimelineCursor
import fe.twwebkt.model.timeline.TimelineEntry
import fe.twwebkt.model.timeline.TimelineInstructionType
import fe.twwebkt.serializer.tweet.TweetDetailInfoSerializer
import fe.twwebkt.serializer.user.TwitterUserSerializer
import mu.two.KotlinLogging

class TimelineSerializer(
    val twitterUserSerializer: TwitterUserSerializer,
    val tweetDetailInfoSerializer: TweetDetailInfoSerializer
) {
    private val logger = KotlinLogging.logger<TimelineSerializer>()

    fun deserialize(instructions: JsonArray?): List<TimelineEntry>? {
        return unwrapEntries(instructions)
            ?.mapNotNull { (entryId, content) -> handleTimelineEntry(entryId, content) }
    }

    private fun handleTimelineEntry(entryId: String, content: JsonObject): TimelineEntry? {
        val lastDash = entryId.lastIndexOf("-")
        val type = entryId.substring(0, lastDash)
        val value = entryId.substring(lastDash + 1)

        logger.info("Handling type $type, value $value")

        val cursorEntry = tryHandleCursor(type, content)
        if (cursorEntry != null) return cursorEntry

        return when (type) {
            "user" -> TimelineEntry.User(value, twitterUserSerializer.handleUser(content))
            "tweet" -> TimelineEntry.Tweet(value, tweetDetailInfoSerializer.handleTweet(content))
            "conversationthread" -> {
                val infos = tweetDetailInfoSerializer.handleConversationThread(content)
                TimelineEntry.ConversationThread(value, infos)
            }

            else -> null
        }
    }

    private fun tryHandleCursor(type: String, content: JsonObject): TimelineEntry? {
        val idx = type.indexOf("-").takeIf { it != -1 } ?: return null
        val prefix = type.substring(0, idx)
        if (prefix != "cursor") return null

        val unwrappedCursor = unwrapCursor(content)

        val cursorType = type.substring(idx + 1)
        return when (cursorType) {
            "bottom" -> TimelineCursor.Bottom(unwrappedCursor)
            "top" -> TimelineCursor.Top(unwrappedCursor)
            "showmorethreads" -> TimelineCursor.MoreThreads(unwrappedCursor)
            else -> throw Exception("Unable to handle cursor of type $cursorType")
        }
    }

    private fun unwrapEntries(instructions: JsonArray?): List<Pair<String, JsonObject>>? {
        return instructions
            ?.mapNotNull { it.objectOrNull() }
            ?.mapNotNull { TimelineInstructionType.find(it.asStringOrNull("type"))?.unwrap(it) }
            ?.flatten()
    }

    private fun unwrapCursor(content: JsonObject): String? {
        return content.asStringOrNull("value") ?: content.asObjectOrNull("itemContent")?.asStringOrNull("value")
    }
}
