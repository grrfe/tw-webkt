package fe.twwebkt.serializer

import java.time.LocalDateTime
import java.time.format.DateTimeFormatter
import java.util.*


object TwitterDateParser {
    // Fri Jul 28 18:21:09 +0000 2023
    // Tue Aug 01 13:33:25 +0000 2023
    private val formatter = DateTimeFormatter.ofPattern("E MMM dd HH:mm:ss Z yyyy", Locale.ENGLISH)

    fun parseOrNull(text: String?): LocalDateTime? {
        if (text == null) return null
        return runCatching { LocalDateTime.parse(text, formatter) }.getOrNull()
    }
}
