package fe.twwebkt.serializer.user

import com.google.gson.JsonObject
import fe.gson.extension.json.`object`.asLongOrNull
import fe.gson.extension.json.`object`.asObjectOrNull
import fe.gson.extension.json.`object`.asStringOrNull
import fe.twwebkt.extension.nestedUnwrap
import fe.twwebkt.extension.unwrapAnyOf
import fe.twwebkt.model.user.TwitterUser
import fe.twwebkt.serializer.TwitterDateParser

class TwitterUserSerializer {
    fun handleUser(response: JsonObject): TwitterUser? {
        val userId = response.asStringOrNull("id_str")
        if (userId != null) {
            // Legacy response, only handle that
            return deserializeLegacy(userId, response)
        }

        val result = unwrapUserResult(response) ?: return null
        return deserialize(result)
    }

    internal fun unwrapUserResult(response: JsonObject): JsonObject? {
        // 1. Single user query
        // 2. TimelineTimelineItem
        // 3. Within tweet received from query by id
        return with(response) {
            nestedUnwrap("data", "user", "result")
                ?: unwrapAnyOf("itemContent", "core")
                    ?.unwrapAnyOf("user_results", "user_result")
                    ?.asObjectOrNull("result")
        }
    }

    internal fun deserialize(result: JsonObject): TwitterUser {
        val userId = result.asStringOrNull("rest_id")
        val legacy = result.asObjectOrNull("legacy")

        return deserializeLegacy(userId!!, legacy)
    }

    internal fun deserializeLegacy(userId: String, legacy: JsonObject?): TwitterUser {
        val screenName = legacy?.asStringOrNull("screen_name")
        val name = legacy?.asStringOrNull("name")
        val description = legacy?.asStringOrNull("description")
        val createdAt = TwitterDateParser.parseOrNull(legacy?.asStringOrNull("created_at"))
        val location = legacy?.asStringOrNull("location")
        val mediaCount = legacy?.asLongOrNull("media_count")
        val statusCount = legacy?.asLongOrNull("status_count")
        val followers = legacy?.asLongOrNull("followers_count")
        val following = legacy?.asLongOrNull("friends_count")


        return TwitterUser(
            userId,
            name!!,
            screenName!!,
            description!!,
            createdAt!!,
            location,
            mediaCount,
            statusCount,
            followers,
            following
        )
    }
}
