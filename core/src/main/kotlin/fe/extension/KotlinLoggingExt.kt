package fe.extension

import mu.two.KLogger
import mu.two.KotlinLogging

inline fun <reified Class> KotlinLogging.logger(): KLogger {
    return logger(Class::class.simpleName!!)
}
