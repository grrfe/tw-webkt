package fe.httpkt.interceptor

fun interface ResponseInterceptor<T> {
    fun onSuccess(response: T)
}

fun <T> IndexedResponseInterceptor(onSuccess: (Int, T) -> Unit): ResponseInterceptor<T> {
    var index = 0
    return ResponseInterceptor<T> { onSuccess(index++, it) }
}
