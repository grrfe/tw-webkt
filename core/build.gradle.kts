import fe.build.dependencies.Grrfe


plugins {
    kotlin("jvm")
    kotlin("plugin.serialization")
    id("net.nemerosa.versioning")
    `maven-publish`
    id("build-logic-plugin")
}

group = "fe.twwebkt"
version = versioning.info.tag ?: versioning.info.full

repositories {
    mavenCentral()
    maven(url = "https://jitpack.io")
}

dependencies {
    api(platform(Grrfe.std.bom))
    api(Grrfe.std.core)
    api(Grrfe.std.time.java)
    api(Grrfe.std.process.core)
    api(Grrfe.std.result.core)
    api(Grrfe.std.uri)

    api(platform(Grrfe.koinApp.bom))
    api(Grrfe.koinApp.core)
    api(Grrfe.koinApp.cli.core)

    api(platform(Grrfe.httpkt.bom))
    api(Grrfe.httpkt.core)
    api(Grrfe.httpkt.gson)
    api(Grrfe.httpkt.jsoup)
    api(Grrfe.httpkt.auth.core)
    api(Grrfe.httpkt.auth.oauth10a)

    api(Grrfe.ext.gson)

    // Logging
    api("org.slf4j:slf4j-simple:_")
    api("io.github.microutils:kotlin-logging-jvm:_")

    // App
    api(Koin.core)

    implementation(KotlinX.serialization.json)
    implementation("com.charleskorn.kaml:kaml:_")

    // Cli
    implementation("com.github.ajalt.clikt:clikt:_")


    // Utils
    implementation(KotlinX.coroutines.core)
    implementation("org.lz4:lz4-java:_")


    implementation("com.google.code.gson:gson:_")
    implementation("org.jsoup:jsoup:_")
    implementation("com.gitlab.grrfe:jsoup-ext:_")

    implementation("com.eatthepath:java-otp:_")
    implementation("io.matthewnelson.encoding:base32:_")

    implementation("io.github.redouane59.twitter:twittered:_")
    testImplementation("com.willowtreeapps.assertk:assertk:_")

    testImplementation(kotlin("test"))
}

tasks.test {
    useJUnitPlatform()
}

publishing {
    publications {
        create<MavenPublication>("maven") {
            groupId = project.group.toString()
            version = project.version.toString()

            from(components["java"])
        }
    }
}
