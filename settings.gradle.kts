import fe.buildsettings.extension.*


pluginManagement {
    repositories {
        mavenCentral()
        gradlePluginPortal()
        maven { url = uri("https://jitpack.io") }
    }

    val gradleBuild = File(rootProject.projectDir.parentFile, "gradle-build").absolutePath
    includeBuild(gradleBuild)

    plugins {
        id("net.nemerosa.versioning") version "3.1.0"
        id("de.fayard.refreshVersions") version "0.60.5"
        kotlin("jvm") version "2.1.0" apply false
    }
}

plugins {
    id("de.fayard.refreshVersions")
    id("build-settings-plugin")
}

val gradleBuild: String = File(rootProject.projectDir.parentFile, "gradle-build").absolutePath
includeBuild(gradleBuild)

rootProject.name = "tw-webkt"

include(":core")

if (true) {
    include(":testing")
    include(":playground")
}

val localProperties = file("local.properties")
val devProperties = localProperties.loadPropertiesOrNull()

val isDev = (devProperties?.get("dev")?.toString()?.toBooleanStrictOrNull() == true)

if (devProperties != null && isDev && (!isCI && !isJitPack)) {
    trySubstitute(devProperties["kotlin-ext.dir"], "com.gitlab.grrfe.kotlin-ext") {
        this["core"] = "core"
        this["io"] = "io"
        this["time-core"] = "time:time-core"
        this["time-java"] = "time:time-java"
        this["result-core"] = "result:result-core"
        this["result-assert"] = "result:result-assert"
        this["uri"] = "uri"
    }

    trySubstitute(devProperties["httpkt.dir"], "com.gitlab.grrfe.httpkt") {
        this["core"] = "core"
        this["ext-gson"] = "ext-gson"
        this["ext-jsoup"] = "ext-jsoup"
        this["auth-core"] = "auth:auth-core"
        this["auth-oauth-10a"] = "auth:auth-oauth-10a"
    }

    trySubstitute(devProperties["gson-ext.dir"], "com.gitlab.grrfe.gson-ext") {
        this["core"] = "core"
        this["koin"] = "koin"
    }

    trySubstitute(devProperties["koin-app.dir"], "com.gitlab.grrfe.koin-app") {
        this["core"] = "core"
        this["api"] = "api"
        this["cli-core"] = "cli:cli-core"
        this["exposed-core"] = "exposed:exposed-core"
        this["exposed-sqlite"] = "exposed:exposed-sqlite"
    }
}
