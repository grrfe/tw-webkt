package fe.twwebkt.runnable

import fe.std.result.isSuccess
import fe.twwebkt.android.TwitterSessionHandle
import fe.twwebkt.model.tweet.TweetDetailInfo
import fe.twwebkt.serializer.DefaultSerializers
import fe.twwebkt.serializer.tweet.TweetDetailInfoSerializer
import fe.twwebkt.task.request.tweet.TweetResultByIdQueryTask

class GetTweetDetailRunnable(
    val tweetId: String,
    private val tweetDetailInfoSerializer: TweetDetailInfoSerializer = DefaultSerializers.tweetDetailInfoSerializer
) : TwitterRunnable<TweetDetailInfo?> {
    override fun run(handle: TwitterSessionHandle): TweetDetailInfo? {
        val result = send(handle, TweetResultByIdQueryTask(tweetDetailInfoSerializer, tweetId = tweetId))
        if (result.isSuccess()) {
            return result.value
        }

        return null
    }
}
