package fe.twwebkt.extension

import mu.two.KLogger
import mu.two.KotlinLogging

inline fun <reified T> KotlinLogging.logger(): KLogger {
    return logger(T::class.java.simpleName)
}
